<?php 

try {
        $modelComprobantes= new Model_Comprobantes_PSE();
        $dataArray = (array) json_decode( $result['data_json'] );//estructura o infomacion 

        $estado = $dataArray['estado'];
        $dataArray = $classEmailReportJob->setStatusCpe( $estado, $dataArray );

        $parameter = setParameterRequestQuery( $dataArray );
        $filters = $modelComprobantes->getWithFilters( $parameter[ 'emi_ruc' ] , $parameter[ 'doc_tipo' ], $parameter[ 'doc_serie' ], $parameter[ 'doc_nume' ] , $parameter[ 'doc_fechini' ], $parameter[ 'doc_fechfin' ]  , $parameter[ 'estados' ], $parameter[ 'nro_documento' ] ,$parameter[ 'status_email' ] , $parameter );

        $parameter = $classEmailReportJob->generateReport( $filters );

        $classEmailReportJob->sendEmail();
        $classEmailReportJob->deleteJob();    
        write_log_job( 'Se envio correctamente el Reporte al email : ' . $dataArray['emails'] . '  Se creo correctamente el archivo :'  . $classEmailReportJob->_fileName ,1);
} catch (Exception $e) {
        write_log_job( 'Error: '.$e->getMessage(). ' Email : ' . $dataArray['emails'] . '  archivo :'  . $classEmailReportJob->_fileName );
}
    