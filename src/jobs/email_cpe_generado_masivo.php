<?php 

try {
        $data_job = $result;
        $listFile = [];
        $listComprobante = [];
        $array_file_not_exist = [];
        $modelComprobantes= new Model_Comprobantes_PSE();
        $dataArray = (array)json_decode( $data_job['data_json'] );//estructura o infomacion 
        $zip = new ZipArchive();
        $dateNow = \date('Ymd_His');
        $zip_name = $dateNow . $dataArray['emi_ruc'] ;
        $decryption_iv = '1234567891011121'; 
        $options = 0; 
        $decryption_key = "cVH62VUtFr8br2qEOQef3KPXMDGMMMSFrRIMzunvlQk";
        $ciphering = "AES-256-CFB1";//metodo de cifrado 
        $parameter = setParameterRequestQuery( $dataArray );
        $filters = $modelComprobantes->getWithFilters( $parameter[ 'emi_ruc' ] , $parameter[ 'doc_tipo' ], $parameter[ 'doc_serie' ], $parameter[ 'doc_nume' ] , $parameter[ 'doc_fechini' ], $parameter[ 'doc_fechfin' ]  , $parameter[ 'estados' ], $parameter[ 'nro_documento' ] ,$parameter[ 'status_email' ] , $parameter );

        $list = $classEmailReportJob->generateReportMasive( $filters );
        if ( count( $list ) > 0 )
        {
                
                if ( $dataArray['metodo'] == 'pdf' )
                {
                        foreach ($list as $key => $value) 
                        {
                                array_push($listComprobante, 
                                        [
                                        'CDR'=>'0',
                                        'tipocpe'=> $value['tipo_doc'],
                                        'extension' => 'pdf',
                                        'fecha' => $value['fechadoc'],
                                        'nombcpe'=> $value['ruc'] .'-'.$value['tipo_doc'].'-'.  $value['num_doc'],
                                ]);
                        }   
                }else
                {
                        foreach ($list as $key => $value) 
                        {
                                array_push($listComprobante, [
                                        'CDR'=>'0',
                                        'tipocpe'=>$value['tipo_doc'],
                                        'extension' => 'pdf',
                                        'fecha' => $value[ 'fechadoc' ],
                                        'nombcpe'=> $value[ 'ruc' ] .'-'.$value[ 'tipo_doc' ].'-'.  $value[ 'num_doc' ],
                                                ]);
                                                array_push($listComprobante, [
                                        'CDR'=>'0',
                                        'tipocpe'=>$value['tipo_doc'],
                                        'extension' => 'cdr',
                                        'fecha' => $value['fechadoc'],
                                        'nombcpe'=> $value['ruc'] .'-'.$value[ 'tipo_doc' ].'-'.  $value[ 'num_doc' ],
                                                ]);
                                                array_push($listComprobante, [
                                        'CDR'=>'0',
                                        'tipocpe'=>$value['tipo_doc'],
                                        'extension' => 'xml',
                                        'fecha' => $value['fechadoc'],
                                        'nombcpe'=> $value[ 'ruc' ] .'-'.$value[ 'tipo_doc' ].'-'.  $value[ 'num_doc' ],
                                ]);
                        }
                }
                foreach ( $listComprobante as $key => $value ) 
                {
                        $fileName = $value[ 'nombcpe' ];
                        $porciones = explode("-", $fileName);   
                        $ruc = $porciones[ 0 ];
                        $formato = $value[ 'extension' ] == 'cdr' ? 'zip' :  $value[ 'extension' ] ;
                        $fecha = $value[ 'fecha' ];
                        $typeDocument =  $porciones[1];
                        $file = get_path_comprobante(  $fileName,  $value[ 'extension' ] ,  $fecha, $ruc , $typeDocument );
                        if ( is_file($file) ) 
                        {
                                $fileElement = [ 'path_file' => $file, 'file_name' =>  $fileName .'.'.$formato , 'formato'=> $formato];//file_name, path_file, formato
                                array_push($listFile , $fileElement );
                        }else
                        {
                                $url = 'https://storage.googleapis.com/xerox_migracion/'.$ruc.'/'.$fileName.'.'.$formato;
                                $pathPdf = verifyAndDown( $url, $fileName, $formato  );
                                if ( $pathPdf !== false ) 
                                {
                                    $fileElement = [ 'path_file' => $pathPdf, 'file_name' =>  $fileName .'.'. $formato,'formato'=>  $formato];
                                    array_push($listFile , $fileElement );
                                }
                        }
                }
                if ( count( $listFile ) > 0 ) 
                {
                        $zip_name  = 'Comprante_compromidos_'. date( 'Y-m-d_H:i:s' );
                        $zip_path = $_SERVER['DOCUMENT_ROOT'].'/ws/temp/'.$zip_name.'.zip';
                        if ( $zip->open( $zip_path, ZIPARCHIVE::CREATE ) ===true ) 
                        {
                                foreach ($listFile as  $value) 
                                {
                                    $zip->addFile( $value[ 'path_file' ] ,  $value[ 'file_name' ] );
                                }
                                $zip->close();
                                $zipname =  $zip_name . '.zip';
                        }
                        $classEmailReportJob->_fileName = $zip_name.'.zip';
                        // $classEmailReportJob->_pathFileReport = $zip_path;
                        $classEmailReportJob->_pathFileReport = $zip_path;
                        $response = $classEmailReportJob->sendEmailZipMasivo( $dataArray['host_dow'] );
                        if ( $response['status'] ) 
                        {
                             write_log_job( 'Se envio correctamente el Reporte al email : ' . $dataArray['emails'] . '  Se creo correctamente el archivo :'  . $classEmailReportJob->_fileName ,1);        
                        }else
                        {
                             write_log_job( 'No se envio correo : ' . $dataArray['emails'] . ' Message: ' . $response['message'] ) ;   
                        }
                }else{
                        echo "no existen archivos";
                }
                $classEmailReportJob->deleteJob();    
                                
        }else{
                echo "no existe data";
        }
} catch (Exception $e) {
        write_log_job( 'Error: '.$e->getMessage(). ' Email : ' . $dataArray['emails'] . '  archivo :'  . $classEmailReportJob->_fileName );
}
    