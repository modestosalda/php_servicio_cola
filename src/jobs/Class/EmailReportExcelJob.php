<?php 

Include_once   $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/Modelos/Model_Job_Client.php';//ruta /var/www/html
// Include_once   $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/Modelos/Model_Comprobantes_PSE.php';//ruta /var/www/html
include '../helpers/common_helper.php';

// include '../config/config_excel.php';
include '../providers/ProviderServiceEmail.php';
include '../providers/ProviderExcel.php';
ini_set('memory_limit', '-1');
ini_set('MAX_EXECUTION_TIME', '400');

/**
 * 
 */
class EmailReportExcelJob 
{
    private $_modelJobClient ;
    private $_typeJob ;
    private $_jobId ;
    private $_dataJob ;
    public $_fileName ;
    public $_pathFileReport ;
    private $_modelComprobantePSE ;
    function __construct(  )
    {
        $this->_modelJobClient = new Model_Job_Client();
        $this->_modelComprobantePSE = new Model_Comprobantes_PSE();
    }
    public function getJobActiveByTypeReport( )
    {
        $result = $this->_modelJobClient->getJobActiveByType(  );
        if ( count( $result )> 0 ) 
        {

            $this->_dataJob = $result;
            $this->_jobId = $result['id'];
            $this->_typeJob = $result['type'];
            $where=[
                'id' => $this->_jobId
            ];
            $data = [
                'status' => 'P'
            ];
            $this->_modelJobClient->CI_UPDATE( $data, $where );
        }
        return $result;
    }

    private function getListCpe( $filters )
    {
        $resultComprobante = $this->_modelComprobantePSE->getCpeReportExcel( $filters );
        return $resultComprobante;
    }

    public function generateReport( $filters )
    {
        $rowIni = '17';
        $columnIni = 'B';
        $headerList = [ 
            'FECHA' => 'fechadoc','SERIE DOC.' => 'serie_correlativo', 'IGV' => 'igv', 'MONTO' => 'monto', 'TIPO DOC.' => 'tipo_doc', 'ESTADO' => 'respre' , 'ADQUIRIENTE' => 'tb_razon_social', 'DOCUMENTO' => 'numeroidentificacioncliente'
        ];
        $columnsSize = [ 'B' => 13, 'F' =>  13, 'G' => 20, 'H' => 35, 'I' => 25 ];
        $fecha1 = date('Ymd');
        $hora = date('His');
        $this->_fileName = "Listado_de_comprobantes_$fecha1-$hora.xls";
        $titleReport = 'Listado de comprobantes';
        $this->_pathFileReport =  $_SERVER['DOCUMENT_ROOT'].'/ws/temp/'.$this->_fileName; // Provide path to your logo file
        $data = $this->getListCpe( $filters );

        $providerExcel = new ProviderExcel( $titleReport , $headerList, $rowIni, $columnIni, $data, $this->_pathFileReport,$columnsSize );
        $providerExcel->saveDoc();
    }
    public function generateReport2( $filters )
    {
        $rowIni = '1';
        $columnIni = 'A';
        $headerList = [ 
            'RUC'=>'ruc', 'RUC ADQUIRIENTE'=>'numeroidentificacioncliente', 'ADQUIRIENTE' => 'tb_razon_social','FECHA' => 'fechadoc', 'TIPO DOC.' => 'tipo_doc','NRO CPE' => 'serie_correlativo', 'MONTO' => 'monto', 'MONEDA' => 'moneda', 'ESTADO CPE' => 'respre', 'ESTADO EMAIL' => 'emailto_status' 
        ];
        $columnsSize = [ 'A' => 13, 'B' => 17, 'C' => 37, 'D' => 15, 'E' => 13, 'F' =>  15, 'G' => 10, 'H' => 10, 'I' => 11,'J' => 12 ];
        $fecha1 = date('Ymd');
        $hora = date('His');
        $this->_fileName = "Listado_de_comprobantes_$fecha1-$hora.xls";
        $titleReport = 'Listado de comprobantes';
        $this->_pathFileReport =  $_SERVER['DOCUMENT_ROOT'].'/ws/temp/'.$this->_fileName; // Provide path to your logo file
        $data = $this->getListCpe( $filters );

        $providerExcel = new ProviderExcel( $titleReport , $headerList, $rowIni, $columnIni, $data, $this->_pathFileReport,$columnsSize );
        $providerExcel->saveDoc();
    }

    public function setStatusCpe( $estado , $dataArray )
    {
        switch ( $estado ) 
        {
            case 'A':
                    $dataArray[ 'pb_a' ] = true;
                break;
            case 'O':
                    $dataArray[ 'pb_o' ] = true;
                break;
            case 'R':
                    $dataArray[ 'pb_r' ] = true;
                break;
            case 'B':
                    $dataArray[ 'pb_b' ] = true;
                break;
            
            default:
                    $dataArray[ 'pb_a' ] = true;
                    $dataArray[ 'pb_o' ] = true;
                    $dataArray[ 'pb_r' ] = true;
                    $dataArray[ 'pb_b' ] = true;
                break;
        }
        return $dataArray;
    }


    public function generateReportMasive( $filters )
    {
        $list =  $this->_modelComprobantePSE->getCPEExcel($filters);

        return $list;
    }
    function deleteJob()
    {
        $this->_modelJobClient->deleteJob( $this->_jobId );
    }
    function sendEmail()
    {
        include '../Config/config.php';
        $dataArray = (array) json_decode( $this->_dataJob['data_json'] );
        $toEmail = $dataArray['emails'];
        $subject = 'Reporte Comprobates Emitidos';
        $sender = get_subjet_by_ruc();
        $files = [];
        $bodyHtml=null;
        $deleted_is = false;
        if ( filesize( $this->_pathFileReport ) <= 15000000 ) 
        {
            $deleted_is = true;
            $files = [[
                'file_name' => $this->_fileName,
                'path_file' => $this->_pathFileReport
            ]];    
        }else{
            $bodyHtml = "<h2>Puede descargar su reporte con el siguiente link:</h2>"
            . "<a href='" . $config['url_base'].'/ws/apiv1/dowload_report.php?name_file=' . $this->_fileName ."'>Descargar Reporte</a>"
            . "<br>"
            . "<h3>Reporte:</h3><br>"
            .  "Nombre: ".$this->_fileName;
             $bodyHtml .= '<br><br><h3>Una vez descargue su archivo, se eliminara</h3>';
        }
        $providersEmail = new ProviderServiceEmail( $toEmail, $sender, $subject, $files , $bodyHtml , $header = null, $typeEmail = null );
        $responseEmail = $providersEmail->sendEmail();
        if ( $deleted_is ) 
        {
            unlink( $this->_pathFileReport );
        }
        return $responseEmail;
    }
    function sendEmailZipMasivo( )
    {
        include '../Config/config.php';
        $dataArray = (array) json_decode( $this->_dataJob['data_json'] );
        $toEmail = $dataArray['emails'];
        $subject = "..:Documentos Comprimidos:.." ;
        $subject = 'Reporte Comprobates Emitidos';
        $sender = get_subjet_by_ruc( $dataArray['emi_ruc'] );
        $files = [];
        $bodyHtml=null;
        $deleted_is = false;
        $url_path = $config['url_base'] . "/ws/apiv1/dowload_report.php?name_file=" . $this->_fileName ."&formato=zip";
        $bodyHtml = "<h2>Puede descargar su archivo :</h2>"
                    . "<a href='" .  $url_path . "'>Descargar Archivo</a>"
                    . "<br>"
                    . "<h3>Sus comprobantes electronicos:</h3><br>"

                    .  "Nombre: ".$this->_fileName
                    .  "<br><br>"
                    .  "<p>Si no puede deacargar su archivo electronico copie y peque este link en el navegador:" . $url_path
                    ;
        $providersEmail = new ProviderServiceEmail( $toEmail, $sender, $subject, $files , $bodyHtml , $header = null, $typeEmail = null );
        $responseEmail = $providersEmail->sendEmail();
        if ( $deleted_is ) 
        {
            unlink( $this->_pathFileReport );
        }
        return $responseEmail;
    }
}

