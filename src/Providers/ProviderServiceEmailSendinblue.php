<?php 

namespace Providers;
use SendinBlue;
/**
 * 
 */
class ProviderServiceEmailSendinblue 
{
	private $_config_service ;
	private $_config ;
	private $_sendSmtpEmail ;
	private $_toEmail ;
	private $_sender ;
	private $_subject ;
	private $_bodyHtml ;
	private $_files ;// [ 'file_name' , 'path_file', 'formato', 'base64' ]
	private $_header ;
	private $_typeEmail ;
	private $_listEmail ;
	private $_listFile ;
	private $_listFileSend ;
	private $_apiInstance ;
	private $_emailValidate;
	private $_emailError;
	/**
	* 
	* $toEmail
	* $subject Asunto
	*/
	function __construct( $toEmail, $sender, $subject, $files , $bodyHtml = null, $header = null, $typeEmail = null)
	{
		$this->_files = $files;
		$this->_toEmail = $toEmail;
		$this->_sender = $sender;
		$this->_subject = $subject;
		$this->_typeEmail = $typeEmail;
		$this->_header = $header;
		$this->_bodyHtml = $bodyHtml;
		$this->_listEmail = [];
		$this->_listFile = [];
		$this->_emailValidate = '';
		$this->_emailError = '';
		$this->_config = get_config();
		$this->conex();
		$this->prepareToEmail();
		$this->prepareFile();
		$this->prepareBody();
	}
	/**
	* realiza la conexcion a la api del servicio
	*/
	private function conex()
	{

		$this->_config_service = \SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', $this->_config[ 'token_email_sendinblue' ]); 
		$this->_apiInstance = new \SendinBlue\Client\Api\TransactionalEmailsApi(
            // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
            // This is optional, `GuzzleHttp\Client` will be used as default.
            new \GuzzleHttp\Client(),
            $this->_config_service
        );
        $this->_sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
	}
	/**
	* setea los campos necesario de la direcciones de correos
	*/
	private function prepareToEmail()
	{
		$emails = str_replace(' ', '', $this->_toEmail) ;
		$arrayEmail =  array_unique(  array_filter( explode(",", $this->_toEmail)  ) );
        $emailError = "";
        foreach ($arrayEmail as $value) 
        {
            $emailTo = trim( $value );
            //valida si es un email correcto
            if (filter_var($emailTo, FILTER_VALIDATE_EMAIL) ) 
            {
                $elementEmail = ['email'=> $emailTo , 'name'=>'Cliente'];
                array_push($this->_listEmail, $elementEmail );
                $this->_emailValidate .=" ," .$emailTo;

            }else
            {
            	$this->_emailError .= ", " . $emailTo;
            }
        }

	}
	/**
	* setea todas las configuraciones necesarias para enviar
	*/
	private function prepareConfigParamEmail()
	{
		if ( count( $this->_listEmail ) > 0) 
		{
			$this->_sendSmtpEmail['sender'] = $this->_sender;
			$this->_sendSmtpEmail['subject'] = $this->_subject ;
			$this->_sendSmtpEmail['headers'] = array('X-Mailin-custom'=>'accept:application/json|content-type:application/json');
			$this->_sendSmtpEmail['htmlContent'] = $this->_bodyHtml ;
			$this->_sendSmtpEmail['to'] = $this->_listEmail;
			if ( count( $this->_listFile ) > 0) 
			{
				$this->_sendSmtpEmail['attachment'] = $this->_listFile;
			}
			return true;
		}
		return ['status' => false,'message' => 'Emails invalidos' , 'cod_error' => 404,'code' => 404, 'error' => 'Emails invalidos: ' .$this->_emailError];
		
	}
	/**
	* setea la plantilla html del correo
	*/
	private function prepareBody()
	{
		$this->_bodyHtml = $this->_bodyHtml == null ?  "<h2>Puede descargar todos su Lista de comprobantes emitidos</h2>" : $this->_bodyHtml;
	}
	/**
	* setea los archivos a enivar
	*/
	private function prepareFile()
	{
		foreach ( $this->_files as $key => $value ) 
		{
			if (  !isset( $value[ 'base64' ] ) )
			{
				if ( file_exists( $value['path_file'] ) && filesize( $value['path_file'] ) <= 15000000 ) 
				{
					$value['base64'] = base64_encode( file_get_contents(  $value['path_file']  ) );
					$fileElement = [ 'content' => $value['base64'] , 'name' => $value['file_name'] ];
					array_push($this->_listFile, $fileElement);
				}
			}else{
				$fileElement = [ 'content' => $value['base64'] , 'name' => $value['file_name'] ];
				array_push($this->_listFile, $fileElement);
			}
		}
		$this->_listFileSend = $this->_listFile;
	}
	public function sendEmail()
	{
		$message = [];
		$status = $this->prepareConfigParamEmail();
		
		if ( $status === true )
	    {
			$response = $this->_apiInstance->sendTransacEmail( $this->_sendSmtpEmail );
			
			$statusCodeEmail = $response->getMessageId() ? '202': '';
			if ( $statusCodeEmail == "202" ) 
		    {
		    	$message['message'] = 'se envio correctamente';
		    	$message['info'] = 'se envio correctamente';
		    	$message['status'] = true;
		    	$message['to_emails'] = $this->_emailValidate;
		    	$message['to_emails_invalid'] = $this->_emailError;
		    	$message['files'] = implode(', ',  array_map(function( $element ){ return $element['name']; }, $this->_listFileSend )  )  ;
		    	$message['code'] = $statusCodeEmail;
		    }else
		    {

		    	$message['status'] = false;
		    	$message['info'] = 'Ocurrio un error';
		    	$message['cod_error'] = $statusCodeEmail;
		    	$message['code'] = $statusCodeEmail;
		    }
		    return $message;
		}
		return $status;
	}
	public function sendEmailByPlantilla( $record )
	{

	}


}

 ?>