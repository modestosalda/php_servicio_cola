<?php 
require_once '../Classes/PHPExcel.php';
ini_set('max_execution_time', 360);
/**
 * 
 */
class ProviderExcel 
{
	private $_objPHPExcel ;
	private $_title ;
	private $_headerList ;
	private $_rowIni ;
	private $_columnIni ;
	private $_columnEnd ;
	private $_data ;
	private $_columnsSize;
	private $_pathFile;
	function __construct( $title , $headerList, $rowIni, $columnIni, $data, $pathFile, $columnsSize = [])
	{
		$this->_title = $title;
		$this->_headerList = $headerList;
		$this->_rowIni = $rowIni;
		$this->_columnIni = $columnIni;
		$this->_columnEnd = '';
		$this->_data = $data;
		$this->_columnsSize = $columnsSize;
		$this->_pathFile = $pathFile;
		$this->init();
		
	}
	private function init()
	{
		$this->_instaceLib();
		$this->prepareHeaderList();
		//$this->styleDefaultHeaderInfo();
		$this->setRowData();
	}
	private function _instaceLib()
	{
		$this->_objPHPExcel = new PHPExcel();
		$this->_objPHPExcel->getProperties()->setCreator("ZetaGas")
	        ->setLastModifiedBy("ZetaGas")
	        ->setTitle("Office 2007 XLSX Test Document")
	        ->setSubject("Office 2007 XLSX Test Document")
	        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
	        ->setKeywords("office 2007 openxml php")
	        ->setCategory("Reporte");
	    $this->_objPHPExcel->getActiveSheet()->setTitle($this->_title);
	}
	private function prepareHeaderList()
	{
		include '../Config/config_excel.php';
		$column = $this->_columnIni;
		$headerList = array_keys( $this->_headerList );
		foreach ($headerList as  $value) 
		{
			$this->_objPHPExcel->getActiveSheet()->SetCellValue($column.$this->_rowIni, $value);
			$column++;
		}
		$this->_columnEnd = $column--;
		$paramStyle = $this->_columnIni . $this->_rowIni. ':'.$this->_columnEnd .$this->_rowIni;
		$this->_objPHPExcel->getActiveSheet()->getStyle($paramStyle)->applyFromArray($this->setColorFontSize(true, 'FFFFFF', '7', 'Arial'));
	    $this->_objPHPExcel->getActiveSheet()->getStyle($paramStyle)->applyFromArray($fondoNegro);
	    $this->_objPHPExcel->getActiveSheet()->getStyle($paramStyle)->applyFromArray($bordesTodos);
	    $this->_objPHPExcel->getActiveSheet()->getStyle($paramStyle)->applyFromArray($aligCenter);
	}
	function setColorFontSize($bold, $color, $size, $familiy) {
	    $tipoColorFont = array(
	        'font' => array(
	            'bold' => $bold,
	            'color' => array('rgb' => $color),
	            'size' => $size,
	            'name' => $familiy
	    ));
	    return $tipoColorFont;
	}
	private function styleDefaultHeaderInfo()
	{
		include '../Config/config_excel.php';
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		// $objDrawing->setName('Logo');
		// $objDrawing->setDescription('Logo');
		$this->_objPHPExcel->getDefaultStyle()->applyFromArray($sinBordes);
	    $this->_objPHPExcel->getActiveSheet()->mergeCells('B9:M9');
	    $this->_objPHPExcel->getActiveSheet()->getStyle('B9:M9')->applyFromArray($fondoNegro);
	    $this->_objPHPExcel->getActiveSheet()->getStyle('B9:M9')->applyFromArray($bordesTop);
	    $this->_objPHPExcel->getActiveSheet()->getStyle('B9:M9')->applyFromArray($aligCenter);
	    $this->_objPHPExcel->getActiveSheet()->getStyle('B9:M9')->applyFromArray($this->setColorFontSize(true, 'FFFFFF', '14', 'Arial'));
	    $this->_objPHPExcel->getActiveSheet()->getStyle('B9:B15')->applyFromArray($bordesIzq);
	    $this->_objPHPExcel->getActiveSheet()->getStyle('M9:M15')->applyFromArray($bordesDer);
	    $this->_objPHPExcel->getActiveSheet()->getStyle('B15:M15')->applyFromArray($bordesBottom);
   		$this->_objPHPExcel->getActiveSheet()->SetCellValue('B9', $this->_title);


	}
    private function setRowData()
    {
		include '../Config/config_excel.php';
    	$fila = $this->_rowIni + 1 ;
    	foreach ($this->_data as  $itemData) 
    	{
    		$column = $this->_columnIni;
    		foreach ($this->_headerList  as $value) 
			{

				if ( $value == 'tipo_doc' ) 
				{
					$itemData[ $value ] = get_type_doc_by_type( $itemData[ $value ] );
				}
				if ( $value == 'respre' ) 
				{
					$itemData[ $value ] = get_name_status_by_respe( $itemData[ $value ] );
				}
				$this->_objPHPExcel->getActiveSheet()->SetCellValue( $column . $fila, $itemData[ $value ]);
				$column++;
			}
			$fila ++;
    	}
    	$headerList = array_keys( $this->_headerList );
    	$column = $this->_columnIni;
    	foreach ($headerList as $key => $value) 
    	{
    		if ( isset( $this->_columnsSize[ $column ] ) ) 
    		{	
    			$this->_objPHPExcel->getActiveSheet()->getColumnDimension( $column )->setWidth( $this->_columnsSize[ $column ] );
    		}
			$column++;
    	}
    }
	public function saveDoc()
	{
    	$objPHPExcels = PHPExcel_IOFactory::createWriter($this->_objPHPExcel, 'Excel2007');
  		$objPHPExcels->save($this->_pathFile);
  		echo "se guardo correctamente";
  		return true;
	}
}

 ?>