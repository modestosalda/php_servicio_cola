<?php 
namespace Providers\Client;
/**
 * 
 */
class ClientService
{
	public $_xmlPostString ;
	public $_header ;
	public $_url ;
	public $_response ;
	public $_statusCode ;
	public $_serviceType ;
	public $_messageError = false;

	function __construct(  $serviceType, $url )
	{
		$this->_url = trim( $url );
		$this->_serviceType = $serviceType;
	}

	public function setResposne( $response )	{ $this->_response = $response;	}
	public function getResponse(){ return $this->_response ;}

	public function setStatusCode( $statusCode )	{ $this->_statusCode = $statusCode;	}
	public function getStatusCode(){ return $this->_statusCode ;}

	public function setError( $messageError )	{ $this->_messageError = $messageError;		// write_log( $messageError . 'Object : ' get_class(), 'error' );
	}
	public function getError(){ return $this->_messageError; }

	public function prepareService( $object )
	{
		
		switch ( $this->_serviceType ) 
		{
			case 'sendSummary':
					$result =  $object->getBody();
					$this->_xmlPostString = $result['body'] ;
					$this->_header = $result['header'] ;
				break;
		}
	}

	public function valideUrl()
	{
		$urlParts = parse_url( $this->_url );
		$scheme = $urlParts[ 'scheme' ];
		if ( $scheme !== 'https' )
		{
			$message = 'URL invalida: ' . $this->scheme;
			$this->setError( $message );
			return false;
		}
		return true;
	}
	public function consume()
	{
		$logs = [];
		if ( !$this->valideUrl() )
		{
			return ['status' => false , 'message' => 'url invalida'];
		}
		
		$logs['start_curl'] = "Iniciando envio curl: ". date('Y-m-d H:m:s.').round(microtime(true) * 1000);
		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_URL, trim( $this->_url ));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_xmlPostString); // the SOAP request
	    if ( count( $this->_header ) ) 
	    {
	    	curl_setopt($ch, CURLOPT_HTTPHEADER, $this->_header );	
	    }
	    $this->setResposne( curl_exec($ch) ) ;
	    $this->setStatusCode( curl_getinfo($ch, CURLINFO_HTTP_CODE) ) ;
	    curl_close($ch);
		$logs['end_curl'] = "fin envio curl: ". date('Y-m-d H:m:s.').round(microtime(true) * 1000);
	    if ( !$this->getResponse() ) 
	    {
	    	$message = json_encode([
	    		'URL' => $this->_url,
	    		'CURL_ERROR' => curl_error($ch),
	    		'STATUS_CODE' => $this->getStatusCode(),
	    		'BODY' => $this->_xmlPostString,
	    		'HEADER' => $this->_header,
	    		'RESPONSE' => $this->getResponse(),
	    	], JSON_UNESCAPED_SLASHES );

	    	$this->setError(  $message );
	    	return ['status' => false , 'response' => $message  , 'logs' => $logs] ;

	    }
	    $response = $this->response();
	    $response['logs'] = $logs;
	    return $response;
	}
	/***
	* preapra el response dado el status Code de la respuesta del servicios
	*/
	public function response()
	{
		if ( $this->getStatusCode() == '200') 
		{
			return	[ 'status' => true, 'response' => $this->getResponse(), 'status_code' => $this->getStatusCode() ];
		}
		return	[ 'status' => false , 'response' => $this->getResponse(), 'status_code' => $this->getStatusCode() ];
	}
}
 