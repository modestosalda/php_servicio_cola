<?php 
namespace Providers\Client;
use Models\Model_Comprobantes_PSE;
/**
 * 
 */
class SendSummary 
{
	
	private $_user;
	private $_pass;
	private $_base64zip;
	private $_fileName;
	private $_serie;
	private $_ruc;
	private $_correlativo;
	private $_cpeType;
	private $_modelComprobanteCpe;
	private $_comprobanteId;
	private $_fechaEmision;
	public function __construct( $fileName )
	{
		$porciones = explode('-',  explode('.', $fileName)[0] );
		$this->_ruc = $porciones[ 0 ];
		$this->_cpeType = $porciones[ 1 ];
		$this->_serie = $porciones[ 2 ];
		$this->_correlativo = $porciones[ 3 ];
	}
	public function setUser( $user )	{		$this->_user = trim($user);	}
	public function getUser(  )	{	return	$this->_user ;	}

	public function setPassword( $pass )	{		$this->_pass = trim($pass);	}
	public function getPassword(  )	{	return	$this->_pass ;	}

	public function setBase64( $base64 )	{		$this->_base64zip = trim($base64);	}
	public function getBase64(  )	{	return	$this->_base64zip ;	}

	public function setFileName( $fileName )	{		$this->_fileName = trim($fileName);	}
	public function getFileName(  )	{	return	$this->_fileName ;	}


	public function getBody()
	{
		$body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
										  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe"
										  xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
								  <soapenv:Header>
									  <wsse:Security>
										  <wsse:UsernameToken>
											  <wsse:Username>' . $this->getUser() . '</wsse:Username>
											  <wsse:Password>' .  $this->getPassword() . '</wsse:Password>
										  </wsse:UsernameToken>
									  </wsse:Security>
								  </soapenv:Header>
								  <soapenv:Body>
									  <ser:sendBill>
										  <fileName>' .  $this->getFileName() . '</fileName>
										  <contentFile>' .$this->getBase64() . '</contentFile>
									  </ser:sendBill>
								  </soapenv:Body>
								  </soapenv:Envelope>';

		$header =  [
							        "Content-type: text/xml;charset=\"utf-8\"",
							        "Accept: text/xml",
							        "Cache-Control: no-cache",
							        "Pragma: no-cache",
							        "SOAPAction:  urn:sendBill",
						        	"Content-length: " . strlen($body),
					];
		return ['body' => $body, 'header' => $header ];
	}


	public function setDataXmlResponse( $xmlResponse, $record )
	{
		$result = [];
    	$result[ 'status' ]  = true;
		$config = get_config();
		$jsonEntrada = json_decode( $record ['data_cpe'] );
		$fechaEmision =  substr($jsonEntrada->fechaEmision, 0, 10 ) ;
		$doc = new \DOMDocument();
        $doc->loadXML( $xmlResponse );
        $result['path_file_cdr'] = '';
		$result['path_file_xml'] = '';
        //===================VERIFICAMOS SI EXISTE ETIQUETA DE RESPUESTA=====================
        if (isset($doc->getElementsByTagName('applicationResponse')->item(0)->nodeValue) ) 
        {
        	$xmlCDR = $doc->getElementsByTagName('applicationResponse')->item(0)->nodeValue;
        	$fileNameCDR = 'R-' . $this->getFileName();
        	try {
        		// obtiene la ruta donde se alojara el cdr
        		$pathCDR = get_path_comprobante(  '' ,  'cdr' ,  $fechaEmision, $this->_ruc, $this->_cpeType );
        		$pathXMLENVIO = get_path_comprobante(  str_replace('.zip', '', $this->getFileName() )  ,  'xml' ,  $fechaEmision, $this->_ruc, $this->_cpeType );
        		if ( isset($config['env']) && $config['env'] == 'local' )
				{
					$pathXMLENVIOZIP = get_path_comprobante(  str_replace('.zip', '', $this->getFileName() )  ,  'zip' ,  $fechaEmision, $this->_ruc, $this->_cpeType );
					file_put_contents( $pathXMLENVIOZIP , base64_decode( $record['base64zip'] ) );
	        		$destineXMLENVIO = get_path_comprobante(  ''  ,  'xml' ,  $fechaEmision, $this->_ruc, $this->_cpeType );
					extract_to_zip( $pathXMLENVIOZIP, $destineXMLENVIO );	
				}
        		

        		//crea el directorio si no existe
        		create_new_directory( $pathCDR );
        		//concatenamos la ruta con el nombre del cdr
        		$pathFileCDR = $pathCDR . $fileNameCDR;
        		//guarda el cdr
        		file_put_contents( $pathFileCDR , base64_decode( $xmlCDR ) );
        		//extrae el xml del zip
				extract_to_zip( $pathFileCDR, $pathCDR );
				
				$pahCDR_XML = $pathCDR . str_replace('zip', 'xml', $fileNameCDR ) ;
				$base64XMLCDR = base64_encode( file_get_contents(   $pahCDR_XML ) );
				$dataXml  = $this->getDataXml( $pahCDR_XML );
				$result[ 'data_response' ] = $dataXml;
				if ( $dataXml[ 'sinProcesar' ]  ) 
		        {
		        	$result[ 'message' ] = 'El documento se manda a reprocesar';
		        	return $result;
		        }
		        $result['path_file_cdr'] = $pathFileCDR;
		        $result['path_file_xml'] = $pathXMLENVIO;
            	return $result;
        	} catch (Exception $e) {
        		return $e->getMessage();
        	}
        }
        $result[ 'informacion' ] = 'RECHAZO DEL SERVICIO, NO EXISTE ETIQUETA DE RESPUESTA';
        /**OBTENEMOS LA INFORMACION DEL RECHAZO*/
        $dataXml =  $this->getDataRechazoXML( $doc );

        $result[ 'data_response' ] = $dataXml;
        if ( $dataXml[ 'sinProcesar' ]  ) 
        {
        	$result[ 'message' ] = 'El documento se manda a reprocesar';
        }
		return $result;
	}

	/**
	* Obtenemos informacion del rechazo
	*/
	private function getDataRechazoXML( $doc )
	{
		$responseCode = '';
		$description = '';
		$sinProcesar = false;
		if ( isset( $doc->getElementsByTagName( 'detail' )->item(0)->nodeValue ) )
		{
			$responseCode = $doc->getElementsByTagName('faultstring')->item(0)->nodeValue;
			foreach ($doc->getElementsByTagName('detail') as $nodo_detail) 
			{	
				$description = $nodo_detail->getElementsByTagName('message')->item(0)->nodeValue;
			}
		}else 
		{
			$responseCode = preg_replace('/[^0-9]+/', '', $doc->getElementsByTagName('faultcode')->item(0)->nodeValue) ;
			$description = $doc->getElementsByTagName('faultstring')->item(0)->nodeValue;
		}
		if ( verify_cod_service_reprocess( $responseCode ) ) 
		{
			$sinProcesar = true;
		}
		return [ 'responseCode' => $responseCode, 'description'=> $description , 'sinProcesar' => $sinProcesar, 'responseService'=> 'R'];

	}
	/**
	* obtiene la data del xml parseada a array
	*/
	private function getDataXml( $base64XML )
	{
		$doc_cdr = new \DOMDocument();
	    $doc_cdr->load( $base64XML );
	    $responseCode = $doc_cdr->getElementsByTagName( 'ResponseCode' )->item(0)->nodeValue;
	    $description = str_replace("'"," ",$doc_cdr->getElementsByTagName('Description')->item(0)->nodeValue);
	    $digestValue = $doc_cdr->getElementsByTagName('DigestValue')->item(0)->nodeValue;
	    $status = NULL;
	    $sinprocesar = false;
		$obsService = '';
		$desexcService = '';
		$valor_notas = [];
		$nota = $doc_cdr->getElementsByTagName('Note');
		foreach ($nota as $notas) 
		{
			$valor_notas[] = $notas->nodeValue ;
		}
		$obsService = str_replace( "'"," ", implode( ";",$valor_notas ) );
		if ( isset( $doc_cdr->getElementsByTagName('Status')->item(0)->nodeValue ) ) 
		{
			$status = $doc_cdr->getElementsByTagName('Status')->item(0)->nodeValue;
			$obs = $doc_cdr->getElementsByTagName('Status');
			$obsArray = [];
			foreach ($obs as $key => $value) 
			{
				array_push($obsArray, $doc_cdr->getElementsByTagName('Status')->item( $key )->getElementsByTagName('StatusReasonCode')->item(0)->nodeValue);
				array_push($obsArray, $doc_cdr->getElementsByTagName('Status')->item( $key )->getElementsByTagName('StatusReason')->item(0)->nodeValue);
			}
			$obsService = str_replace("'"," ",implode("|",$obsArray));
		}
		if($responseCode == 0 && $status == NULL  )//ACEPTADO
		{ 
			$responseService = "A";
		}elseif ( $responseCode == 0 && $status != NULL ) //OBSERVADO
		{
			$responseService = "O";
		}elseif( $responseCode != 0 )//RECHAZADO
		{
			$responseService = "R";
			if( verify_cod_service_reprocess( $responseCode ) )
			{
				$sinprocesar = true;
			}else{
				$desexcService = $description;
			}
		}
		return [ 'description' => $description, 'responseCode' => $responseCode,  'responseService' => $responseService, 'obsService' => $obsService , 'desexcService' => $desexcService , 'sinProcesar' => $sinprocesar];

	}
}