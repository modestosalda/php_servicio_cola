<?php 
namespace Providers;

use Providers\Client\ClientService;
use Providers\Client\SendSummary;
/**
 * 
 */
// class ProviderServiceSunatOse extends ClientService
class ProviderServiceSunatOse extends ClientService
{
	private $_record;
	function __construct( $type, $url, $record )
	{

		 parent::__construct( $type, $url );
		 $this->_record = $record;
	}

	public function requestService( )
	{
		switch ( $this->_serviceType ) 
		{
			case 'sendSummary':
					return $this->requestSummary();
				break;
		}
	}
	private function requestSummary( )
	{
		$record = $this->_record;
		try {

			$summary = new SendSummary( $record[ 'tb_comprobante_nomextarccomele' ] );
			$summary->setUser( $record[ 'tb_emisor_user_sol' ] ) ;
			$summary->setPassword( $record[ 'tb_emisor_clave_sol' ] );
			$summary->setBase64( $record[ 'base64zip' ] );
			$summary->setFileName( $record[ 'tb_comprobante_nomextarccomele' ] );
			/**peticion curl*/
			$this->prepareService( $summary );
			$result = $this->valideConsume(  $this->consume() );
			if ( $result[ 'status' ] ) 
			{	
				$result['curl_sendsummary'] = $result[ 'logs' ];
				$response = $summary->setDataXmlResponse( $result[ 'response' ], $record );
				$result['response_cpe'] = $response;

				// write_log( json_encode( $logs, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES ) , 1 );
			}else{
				write_log( json_encode( $result, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES )  );
			}
			return $result;
		} catch (Exception $e) 
		{
			throw new Exception("Error Processing Request", 1);
					
		}
	}
	
	private function valideConsume( $consume )
	{
		if ( $this->getError() !== false ) 
		{
			return [ 'status' => false, 'message' => 'ocurrio un error' , 'error' => $this->getError() ];
		}
		return $consume;
	}
}