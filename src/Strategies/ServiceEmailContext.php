<?php 
namespace Strategies;
/**
 * 
 */
class ServiceEmailContext 
{
	private $service ;
	function __construct($_service)
	{
		$this->service = $_service;
	}
	public function instaceService( $toEmail, $sender, $subject, $file_cpe , $bodyHtml )
	{
		$className = ucfirst( $this->service );
		$objectClass = "\Providers\ProviderServiceEmail".$className;
		return new $objectClass( $toEmail, $sender, $subject, $file_cpe , $bodyHtml );
	}
}