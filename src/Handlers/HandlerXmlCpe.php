<?php 
namespace Handlers;
/**
 * Clase se encarga de cargar y setear la informacion del xml
 * funciona para tipos de documentos "FACTURA, BOLETA, NOTA DE CREDITO Y NOTA DE DEBITO"
 */
class HandlerXmlCpe
{
	private $xmlObj ;
	private $tipoCpeDoc;
	function __construct($ruta = '', $tipoCpe = '') 
	{
		if ( $ruta != '' ) 
		{
			$file = file_get_contents( $ruta);
			$this->xmlObj =  simplexml_load_string($file);	
			
		}
		$this->tipoCpeDoc = $tipoCpe;
	}
	/**
	* seta la data del xml y retorna un array
	*/
	function setDataXml()
	{
		$tipoCpe = $this->tipoCpeDoc;
		$xml =  $this->xmlObj;

		######################################     DATOS EMISOR  ###################################################
		$ruc_emisor = $xml->xpath('cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID')[0];
		$tipodocumentoemisor = $xml->xpath('cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID')[0]['schemeID'];
		$codigoTipoDocumento = $tipoCpe ? $tipoCpe : $xml->xpath("cbc:InvoiceTypeCode")[0];

		######################################     DATOS ADQUIRIENTE  ##############################################
		$tipodocumentocliente = $xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID')[0]['schemeID'];
		$numeroDocumentoIdentidad = $xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID')[0];
		$clie_razonsocial = end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cbc:RegistrationName')[0]);
		$clie_ubigeo = isset( $xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:ID')[0]) ? end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:ID')[0]) : '';
		$urbanizacion =isset($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CitySubdivisionName')[0]) ? end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CitySubdivisionName')[0]) : '';
		$distrito = isset($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:District')[0]) ?end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:District')[0]) : "";
		$provincia = isset($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CityName')[0]) ?end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CityName')[0]) : '';
		$cod_pais = isset($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cac:Country/cbc:IdentificationCode')[0]) ? end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cac:Country/cbc:IdentificationCode')[0]) : '';
		$departamento = isset($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CityName')[0]) ? end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CityName')[0]) : '';
		$dir_completa = isset($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cac:AddressLine/cbc:Line')[0]) ? end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cac:AddressLine/cbc:Line')[0]) : '';


		######################################     DETALLES CPE  ###################################################
		$numeroCorrelativo = substr($xml->xpath("cbc:ID")[0], 5, 13);
		$numeroSerie = substr($xml->xpath("cbc:ID")[0], 0, 4);
		$fecha = $xml->xpath("cbc:IssueDate")[0];
		$hora = $xml->xpath("cbc:IssueTime")[0];
		$codigoTipoOperacion = isset($xml->xpath("cbc:InvoiceTypeCode")[0]['listID']) ?end($xml->xpath("cbc:InvoiceTypeCode")[0]['listID']):'';
		$codigoTipoMoneda = end($xml->xpath("cbc:DocumentCurrencyCode")[0]);


		###############################     DATOS GLOBALES IMPUESTOS, BASE ,TOTALES  ###############################
		$montoImpuestoMonedaOriginal = floatval(end($xml->xpath("cac:TaxTotal/cbc:TaxAmount")[0]));
		$montoValorVentaMonedaOriginal = isset($xml->xpath("cac:LegalMonetaryTotal/cbc:LineExtensionAmount")[0]) ?floatval(end($xml->xpath("cac:LegalMonetaryTotal/cbc:LineExtensionAmount")[0])): '';
		if ($tipoCpe == '08') // si es Nota de Debito
		{
			$montoTotalMonedaOriginal = floatval(end($xml->xpath("cac:RequestedMonetaryTotal/cbc:PayableAmount")[0]));
		}else
		{
			$montoTotalMonedaOriginal = floatval(end($xml->xpath("cac:LegalMonetaryTotal/cbc:PayableAmount")[0]));
		}


		###############################     PERCEPCIONES  ###########################################################
		$codigoRegimenPercepcion = isset($xml->xpath("cac:AllowanceCharge/cbc:AllowanceChargeReasonCode")[0]) ? end($xml->xpath("cac:AllowanceCharge/cbc:AllowanceChargeReasonCode")[0]) : '';
		$porcentajePercepcion = isset($xml->xpath("cac:AllowanceCharge/cbc:MultiplierFactorNumeric")[0]) ? end($xml->xpath("cac:AllowanceCharge/cbc:MultiplierFactorNumeric")[0]) : '';
		$montoPercepcionMonedaNacional = isset($xml->xpath("cac:AllowanceCharge/cbc:Amount")[0]) ? end($xml->xpath("cac:AllowanceCharge/cbc:Amount")[0]) : '';
		$montoBasePercepcionMonedaNacional = isset($xml->xpath("cac:AllowanceCharge/cbc:BaseAmount")[0])? end($xml->xpath("cac:AllowanceCharge/cbc:BaseAmount")[0]): '';
		$importeTotalConPercepcionMonedaNacional = isset($xml->xpath("cac:PaymentTerms/cbc:Amount")[0]) ? end($xml->xpath("cac:PaymentTerms/cbc:Amount")[0]) : '';


		###############################     ORDENES DE COMPRA Y GUIAS  ##############################################
		$ordenes_compra =isset($xml->xpath("cac:OrderReference/cbc:ID")[0]) ? end($xml->xpath("cac:OrderReference/cbc:ID")[0]) : '';
		$guia_remi = isset($xml->xpath("cac:DespatchDocumentReference/cbc:ID")[0]) ? end($xml->xpath("cac:DespatchDocumentReference/cbc:ID")[0]) : '';
		$tipo_doc_guiaremi = isset($xml->xpath("cac:DespatchDocumentReference/cbc:DocumentTypeCode")[0]) ?end($xml->xpath("cac:DespatchDocumentReference/cbc:DocumentTypeCode")[0]) : '';


		###############################  DOCUMENTO RELACIONADO  	   ##############################################
		$num_doc_rela = isset($xml->xpath("cac:AdditionalDocumentReference/cbc:ID")[0]) ? end($xml->xpath("cac:AdditionalDocumentReference/cbc:ID")[0]): '';
		$tipo_doc_rela	 = isset($xml->xpath("cac:AdditionalDocumentReference/cbc:DocumentTypeCode")[0]) ? end($xml->xpath("cac:AdditionalDocumentReference/cbc:DocumentTypeCode")[0]):'';
		$estado_doc_rela	 = isset($xml->xpath("cac:AdditionalDocumentReference/cbc:DocumentStatusCode")[0]) ?  end($xml->xpath("cac:AdditionalDocumentReference/cbc:DocumentStatusCode")[0]) : '';
		$num_identidad_doc_rela = isset($xml->xpath("cac:AdditionalDocumentReference/cac:IssuerParty/cac:PartyIdentification/cbc:ID")[0]) ?end($xml->xpath("cac:AdditionalDocumentReference/cac:IssuerParty/cac:PartyIdentification/cbc:ID")[0]): '';
		#############################################################################################################

		$data=
		[
			'datos_emisor' => [
				'ruc_emisor' => strval( $ruc_emisor),
				'tipodocumentoemisor' =>  strval( $tipodocumentoemisor)
			],
			'datos_adquiriente' =>
			[
				'tipodocumentocliente' =>strval( $tipodocumentocliente),
				'numeroDocumentoIdentidad' => strval($numeroDocumentoIdentidad),
				'clie_razonsocial' => strval($clie_razonsocial),
				'clie_ubigeo' => strval($clie_ubigeo),
				'urbanizacion' => strval($urbanizacion),
				'distrito' => strval($distrito),
				'provincia' => strval($provincia),
				'cod_pais' => strval($cod_pais),
				'departamento' => strval($departamento),
				'dir_completa' => strval($dir_completa),
			],
			'detalle_cpe' => [
				'numeroCorrelativo' => strval($numeroCorrelativo),
				'numeroSerie' => strval($numeroSerie),
				'fecha' => strval($fecha),
				'hora' => strval($hora),
				'codigoTipoOperacion' => strval($codigoTipoOperacion),
				'codigoTipoMoneda' => strval($codigoTipoMoneda),
				'codigoTipoDocumento' =>strval( $codigoTipoDocumento),
			],
			'datos_globales' => [
				'montoImpuestoMonedaOriginal' => strval($montoImpuestoMonedaOriginal) == '' ? "0.0" : strval($montoImpuestoMonedaOriginal) ,
				'montoValorVentaMonedaOriginal' => strval($montoValorVentaMonedaOriginal) == '' ? "0.0" : strval($montoValorVentaMonedaOriginal) ,
				'montoTotalMonedaOriginal' => strval($montoTotalMonedaOriginal) == "" ? "0.0" : strval($montoTotalMonedaOriginal),
			],
			'percepcion' => [
				'codigoRegimenPercepcion' => strval($codigoRegimenPercepcion),
				'porcentajePercepcion' => strval($porcentajePercepcion),
				'montoPercepcionMonedaNacional' => strval($montoPercepcionMonedaNacional),
				'montoBasePercepcionMonedaNacional' => strval($montoBasePercepcionMonedaNacional),
				'importeTotalConPercepcionMonedaNacional' => strval($importeTotalConPercepcionMonedaNacional),
			],
			'ordenes_compra_guia' => [
				'ordenes_compra' => strval($ordenes_compra),
				'guia_remi' => strval($guia_remi),
				'tipo_doc_guiaremi' => strval($tipo_doc_guiaremi),
			],
		];
		return $data;
	}
	function setDataXmlCdr( $ruta )
	{
		$file_cdr = file_get_contents($ruta);
		$xml =  simplexml_load_string($file_cdr);
		
		$codigo_estado = strval($xml->xpath('cac:DocumentResponse/cac:Response/cbc:ResponseCode')[0]);
		$fecha = strval($xml->xpath('cbc:IssueDate')[0]);
		$hora = strval($xml->xpath('cbc:ResponseTime')[0]);
		$mensaje = strval($xml->xpath('cac:DocumentResponse/cac:Response/cbc:Description')[0]);
		$data =[
			'estado_er' => $codigo_estado,
			'fecha_er' => $fecha,
			'hora_er' => $hora,
			'mensaje' => $mensaje,
		];
     
		return $data;
	}

	function setDataDetalle(  $num_id )
	{
		$data = [];
		
		$xml =  $this->xmlObj;
		// var_dump($xml);
		// exit();
		$nroOrden = 1;
		foreach ($xml->xpath("cac:InvoiceLine") as $nodo) 
		{
			// OBTIENE UNIDAD DE MEDIDA DEL ITEM			
			// var_dump(  $nodo->xpath("cbc:InvoicedQuantity"));
			$invoicedQuantity = $nodo->xpath("cbc:InvoicedQuantity");					
			$cantidad = $invoicedQuantity[0];
   			$codigoUnidadMedida = $cantidad["unitCode"];
   			$item = $nodo->xpath("cac:Item");
   			$item_cod_sunat = '';
   			$item_cod = '';
   			foreach ($item as $nodoProci) 
   			{
				$Description = $nodoProci->xpath("cbc:Description");
				$item_des = $Description[0];
				//OBTIENE CODIGO DE SUNAT
				foreach ($nodoProci->xpath("cac:CommodityClassification/cbc:ItemClassificationCode") as $nodoClassificationCode) {
					$item_cod_sunat = end($nodoClassificationCode[0]);
				}
				if(strlen($item_cod_sunat) < 1){ $item_cod_sunat = null; }
					//OBTIENE CODIGO DE PRODUCTO
				foreach ($nodoProci->xpath("cac:SellersItemIdentification/cbc:ID") as $nodocbcID) 
				{
					$item_cod = end($nodocbcID[0]);
				}
			
				if(strlen($item_cod) < 1){$item_cod = null;}
			}//endforach item

			foreach ($nodo->xpath("cac:PricingReference/cac:AlternativeConditionPrice/cbc:PriceAmount ") as $nodoPriceAmount) 
			{
   				$PriceAmountXX = floatval( end($nodoPriceAmount[0] ) );
   			}
			// OBTIENE PRECIO DEL PRODUCTO SI IGV	
   			foreach ($nodo->xpath("cac:Price") as $nodoPrice) 
   			{
   				$montoValorVentaUnitarioMonedaOriginal = floatval(end($nodoPrice->xpath("cbc:PriceAmount ")[0]));
   			}
			// OBTIENE 	EL VALOR DE LA VENTA POR ITEM 					
			$montoValorVentaTotalMonedaOriginal = end($nodo->xpath("cbc:LineExtensionAmount")[0]); 
				//codigoTipoAfectacionIGV
			//OBTENCION DE IMPUESTOS
   			$TaxTotal = $nodo->xpath("cac:TaxTotal");
   			foreach ($TaxTotal as $nodoTaxTotal) 
   			{
       			//codigoTipoAfectacionIGV
   				foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cac:TaxCategory/cbc:TaxExemptionReasonCode") as $nodoTaxExemptionReasonCode) 
   				{
   					$TaxExemptionReasonCode = end($nodoTaxExemptionReasonCode[0]);
   				}
        		//montoBaseMonedaOriginal
   				foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cbc:TaxAmount ") as $nodoTaxAmount) 
   				{
   					$montoTotalMonedaOriginalXX = floatval(end($nodoTaxAmount[0]));
   				}
				// CODIGO INTERNACIONAL DEL TRIBUTO						
   				foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode") as $nodoTaxTypeCode) 
   				{
   					$codigoInternacionalXX = end($nodoTaxTypeCode[0]);
   				}
				// BASE IMPONIBLE DEL ITEM							
   				foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cbc:TaxableAmount ") as $nodoTaxableAmount) 
   				{
   					$montoBaseMonedaOriginalXX = floatval(end($nodoTaxableAmount[0]));
   				}
  				//OBTIENE NOMBRE DEL TRIBUTO Y CODIGO DEL TRINBUTO
				foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme") as $nodoTaxScheme) 
				{
   					$nombreXX = end($nodoTaxScheme->xpath("cbc:Name")[0]);
   					$IDXX = end($nodoTaxScheme->xpath("cbc:ID")[0]);
   				}
				// PORCENTAJE DEL TRIBUTO
   				foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cac:TaxCategory") as $nodoTaxCategory) 
   				{
   					$PercentXX = floatval(end($nodoTaxCategory->xpath("cbc:Percent")[0]));
   				}

			}//endforeach taxtotal
			$nroOrden++;
			$arrayData = 
			[
				'num_id' => strval(  $num_id),
				'item_des' =>strval(  $item_des),
				'item_cod' => strval( $item_cod),
				'item_cod_sunat' =>strval(  $item_cod_sunat),
				'codigoUnidadMedida' => strval( $codigoUnidadMedida),
				'cantidad' =>strval(  $cantidad),
				'PriceAmountXX' => strval( $PriceAmountXX),
				'montoValorVentaUnitarioMonedaOriginal' =>strval(  $montoValorVentaUnitarioMonedaOriginal),
				'montoValorVentaTotalMonedaOriginal' => $montoValorVentaTotalMonedaOriginal,
				'TaxExemptionReasonCode' =>strval(  $TaxExemptionReasonCode),
				'montoTotalMonedaOriginalXX' => strval( $montoTotalMonedaOriginalXX),
				'montoBaseMonedaOriginalXX' =>strval( $montoBaseMonedaOriginalXX),
				'nombreXX' =>strval(  $nombreXX),
				'codigoInternacionalXX' =>strval(  $codigoInternacionalXX),
				'IDXX' =>strval(  $IDXX),
				'PercentXX' => strval( $PercentXX),
			];
			array_push($data, $arrayData);
		}//endforeach final
		return $data;
	}

	function setDataNota( $num_id )
	{
		$xml =  $this->xmlObj;
		$data = [];
		foreach ($xml->xpath("cbc:Note") as $nodoNote )
		{
   			$descripcion_nota = isset($nodoNote[0])  ? end($nodoNote[0]): '';
   			$codigo_nota = isset($nodoNote[0]['languageLocaleID']) ? end($nodoNote[0]['languageLocaleID']): '';
   			$arrayData = 
   			[
   				'num_id' => $num_id,
   				'descripcion_nota' => $descripcion_nota,
   				'codigo_nota' => $codigo_nota,

   			];
   			array_push($data, $arrayData);
		}
		return $data;
	}
	function setDataTributo( $num_id )
	{
		$data = [];
		$xml =  $this->xmlObj;
		foreach ($xml->xpath("cac:TaxTotal/cac:TaxSubtotal") as $nodo_tri_general) 
		{
			$montoTotaltributo = floatval(end($nodo_tri_general->xpath("cbc:TaxAmount")[0]));
			$montoBaseMonedaOriginal = floatval(end($nodo_tri_general->xpath("cbc:TaxableAmount")[0]));
			foreach ($nodo_tri_general->xpath("cac:TaxCategory/cac:TaxScheme") as $nodo_TaxScheme)
			{
				$nombre = end($nodo_TaxScheme->xpath("cbc:Name")[0]);	
				$codigoInternacional = end($nodo_TaxScheme->xpath("cbc:TaxTypeCode")[0]);	
				$codigo_impuesto = end($nodo_TaxScheme->xpath("cbc:ID")[0]);	
			}
			$arrayData = 
			[
				'num_id' => $num_id,
				'montoTotaltributo'=> $montoTotaltributo ,
				'montoBaseMonedaOriginal'=> $montoBaseMonedaOriginal ,
				'nombre'=> $nombre ,
				'codigoInternacional'=> $codigoInternacional ,
				'codigo_impuesto'=> $codigo_impuesto ,
			];
			array_push($data, $arrayData);
		}
		return $data;
	}
	/**
	* data de documentos relacionados
	*/
	function setDataDocRela( $num_id =0)
	{
		$tipoCpe = $this->tipoCpeDoc;
		$xml =  $this->xmlObj;
		$data = [];
		if ( $tipoCpe== "08" ||  $tipoCpe== "07" ) 
		{
			foreach ($xml->xpath("cac:BillingReference") as $nodo) 
			{
				foreach ($nodo->xpath("cac:InvoiceDocumentReference") as $nodoInvoice) 
				{
					$num_doc_rela =  $nodoInvoice->xpath("cbc:ID")[0];
					$porciones = explode('-', $num_doc_rela);
					$serie = $porciones[0];
					$correl_rela = $porciones[1];
					$tipo_doc_rela =  $nodoInvoice->xpath("cbc:DocumentTypeCode")[0];
					$estado_doc_rela =  isset( $nodoInvoice->xpath("cbc:DocumentStatusCode")[0] ) ? $nodoInvoice->xpath("cbc:DocumentStatusCode")[0]: '';
					$num_identidad_doc_rela =  isset( $nodoInvoice->xpath("cac:IssuerParty/cac:PartyIdentification/cbc:ID")[0]) ? $nodoInvoice->xpath("cac:IssuerParty/cac:PartyIdentification/cbc:ID")[0]:'' ;
				}
				$arrayData = 
				[
					'num_id' => strval( $num_id ),
					'num_doc_rela'=> strval($num_doc_rela ),
					'serie_rela'=> strval($serie ),
					'correl_rela'=> strval($correl_rela ),
					'tipo_doc_rela'=> strval( $tipo_doc_rela ),
					'estado_doc_rela'=>strval( $estado_doc_rela ),
					'num_identidad_doc_rela'=>strval( $num_identidad_doc_rela) 
				];
				array_push($data, $arrayData);
			}
		}else
		{
			foreach ($xml->xpath("cac:AdditionalDocumentReference") as $nodo) 
			{
				$num_doc_rela =  $nodo->xpath("cbc:ID")[0];
				$porciones = explode('-', $num_doc_rela);
				$serie = $porciones[0];
				$correl_rela = $porciones[1];
				$tipo_doc_rela =  $nodo->xpath("cbc:DocumentTypeCode")[0];
				$estado_doc_rela =  $nodo->xpath("cbc:DocumentStatusCode")[0];
				$num_identidad_doc_rela =  $nodo->xpath("cac:IssuerParty/cac:PartyIdentification/cbc:ID")[0];
				$arrayData = 
				[
					'num_id' => strval( $num_id ),
					'num_doc_rela'=> strval($num_doc_rela ),
					'serie_rela'=> strval($serie ),
					'correl_rela'=> strval($correl_rela ),
					'tipo_doc_rela'=> strval( $tipo_doc_rela ),
					'estado_doc_rela'=>strval( $estado_doc_rela ),
					'num_identidad_doc_rela'=>strval( $num_identidad_doc_rela) 
				];
				array_push($data, $arrayData);
			}
		}

		return $data;
	}
	#############################  DOCUMENTO QUE AFECTA  	   ##############################################
	function setDocumentoAfecta()
	{
		$xml = $this->xmlObj;
		// $tipoCpe = $this->tipoCpeDoc;
		$afect_serie_corre = end($xml->xpath("cac:DiscrepancyResponse/cbc:ReferenceID")[0]);
		$tipo_nc = end($xml->xpath("cac:DiscrepancyResponse/cbc:ResponseCode")[0]);
		$motivo_nc = end($xml->xpath("cac:DiscrepancyResponse/cbc:Description")[0]); 
		$modif_serie_corre = end($xml->xpath("cac:BillingReference/cac:InvoiceDocumentReference/cbc:ID")[0]);
		$modif_tipo_doc = end($xml->xpath("cac:BillingReference/cac:InvoiceDocumentReference/cbc:DocumentTypeCode")[0]);
		$data = [
			'afect_serie_corre'=> $afect_serie_corre,
			'tipo_nc'=> $tipo_nc,
			'motivo_nc'=> $motivo_nc,
			'modif_serie_corre'=> $modif_serie_corre,
			'modif_tipo_doc'=> $modif_tipo_doc,
		];
		return $data;
	}
}

 ?>