<?php
namespace Handlers;
use Models\Model_Factura_Cab_CPE ;
use Models\Model_Nota_Credito_CPE;
use Models\Model_Nota_Debito_CPE;
use Models\Model_Percepcion_CPE;
use Models\Model_Retencion_CPE;
use Models\Model_Comprobantes_PSE;
use Models\Model_Documentos_Referenciados_PSE;
use Models\Model_Job_Info;
use Providers\ProviderServiceSunatOse;
use Models\Model_Correo_Plantilla_PSE;
use Strategies\ServiceEmailContext;
// $file_cola =$directorio_cola. "cola.json";
/**
 * 
 */
class HandlerColaEnvio
{
	private $_modelJob ;
	private $_comprobanteId;
	private $_serie;
	private $_ruc;
	private $_correlativo;
	private $_cpeType;
	private $_config;
	private $serviceEmailContext ;
	function __construct()
	{
		$this->_modelJob = new Model_Job_Info();
		$this->_modelComprobantePSE = new Model_Comprobantes_PSE();
		$this->_config = get_config();
		$serviceEmailCpe = $this->_config['service_email_cola_envio'] ?? 'sendingblue';
		$this->serviceEmailContext = new ServiceEmailContext( $serviceEmailCpe );
	}
	public function setFileName( $fileName )	{		$this->_fileName = trim($fileName);	}
	public function getFileName()	{	return	$this->_fileName ;	}
	/**
	* Envia el cpe a OSE-SUNAT
	*/
	public function sendCpeEmitidos()
	{
		$json_cola =  $this->_modelJob->getJobCpe();
		$response = '';
		$responseEmail= '';
		if ( count( $json_cola ) ) 
		{
			$this->_modelJob->updateJob( $json_cola );
			$logs['start'] = 'Inicio de envio de comprobantes : '. date('Y-m-d H:m:s.').round(microtime(true) * 1000);
			foreach ( $json_cola as  $value ) 
			{
				$url = $value['tb_emisor_ws'];
				$porciones = explode('-',  explode('.', $value['tb_comprobante_nomextarccomele'])[0] );
				$this->_comprobanteId = $value[ 'tb_comprobante_id' ];
				$this->_ruc = $porciones[ 0 ];
				$this->_cpeType = $porciones[ 1 ];
				$this->_serie = $porciones[ 2 ];
				$this->_correlativo = $porciones[ 3 ];
				$this->setFileName( $value[ 'tb_comprobante_nomextarccomele' ] );
				$providersSunatOse = new ProviderServiceSunatOse('sendSummary', $url, $value );
				// crea el servicio y manda la peticion
				$response = $providersSunatOse->requestService();
				$jsonEntrada = json_decode( $value ['data_cpe'] );
				$montoTotal = $this->getMontoByTypeMoneda( $jsonEntrada );
				$value['fecha_emision'] =  substr($jsonEntrada->fechaEmision, 0, 10 ) ;
				$value['numero_identificacion_client'] = $jsonEntrada->oAdquiriente->numeroDocumentoIdentidad;
				$value['monto_total'] = $montoTotal;
				$value[ 'codigo_moneda' ] = $jsonEntrada->codigoTipoMoneda;
				if ( $response[ 'status' ] ) 
				{

					$responseCpe = $response['response_cpe'];
					$statusCpe = $responseCpe['data_response'];
					/**El documento se manda a reprocesar si es true*/
					if ( $statusCpe[ 'sinProcesar' ] ) 
					{
						$this->sinprocesar();
					}else{
						/* ENVIA EL EMAIL **/
						$resultEmail = $this->sendEmailCPE( $jsonEntrada,  $value[ "base64pdf" ], $responseCpe[ 'path_file_xml' ] ,$responseCpe[ 'path_file_cdr' ], $statusCpe[ 'responseService' ] );
						$responseEmail = $resultEmail['message'];
						/** ACTUALIZACION DE CPE */
		            	$result[ 'update_comprobante' ]  = $this->_modelComprobantePSE->updateCompobante_ComprobanteAll_JOB( 
		            		$value,
		            		$statusCpe,
		            		$this->getFileName(), 
		            		$responseEmail[ 'response' ][ 'code' ] , 
		            	);

		            	if ( $statusCpe[ 'responseService' ] == 'A' || $statusCpe[ 'responseService' ] == 'O' ) 
		            	{	
		            		if ( isset( $this->_config[ 'env' ] )  &&  $this->_config[ 'env' ] != 'local') 
		            		{
		            			$res['registro_tablas_cpe']= 'no se registra';
		            		}else{
		            			$res['registro_tablas_cpe']= $this->saveCpe( $responseCpe[ 'path_file_xml' ] , $this->_cpeType , $this->_comprobanteId );	

		            		}
		            		
		            	}
		            	
					}
				}
				$logs['end'] = 'Final de envio de comprobantes : '. date('Y-m-d H:m:s.').round(microtime(true) * 1000);
				$logs['cpe'] = $response;
				$logs['info_email'] = $responseEmail ;
				$resJsonEncode = json_encode( $logs, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES );
				write_log_by_type( $resJsonEncode , 'cola_envio' );
			}
			return $logs;
		}
		return ['status' => 'false', 'message' => 'no existen CPE de envio'];
	}
	/**
	* guarda la inforamcion del xml dada la ruta
	* @param $fileruta ruta del archivo
	* @param $tipoCpe tipo de documento "01", "03", "20"
	* @param $comprobanteId id del comprobante
	*/
	function saveCpe( $FileRuta, $tipoCpe , $comprobanteId = 0)
	{
		try {
				$message['ruta del archivo'] = $FileRuta;
				if ( !file_exists($FileRuta) ) 
				{
					throw new Exception("El archivo XML no existe o no tiene permiso para acceder", 2);
					return false;
				}
				$numId=0;
				
				$modelDocumentosReferenciados = new Model_Documentos_Referenciados_PSE();
				if ( $tipoCpe== '01' || $tipoCpe == '07' || $tipoCpe == '08') 
				{
					$classXmlComprobante = new HandlerXmlCpe( $FileRuta,$tipoCpe );
					$resultArraCab = $classXmlComprobante->setDataXml();
				}elseif( $tipoCpe == '40' || $tipoCpe == '20' )
				{
					$classXmlComprobante = new HandlerXmlCpePerRet( $FileRuta,$tipoCpe );
					$resultArraCab = $classXmlComprobante->setDataXml();
					
				}
				
				switch ($tipoCpe) 
				{
					case '01':// FACTURA
							$modelFacturaCab = new Model_Factura_Cab_CPE();
							$resultArrayDocRela = $classXmlComprobante->setDataDocRela();
							if ( count( $resultArrayDocRela ) )
							{
								$arrayNumDoc = array_column($resultArrayDocRela, 'num_doc_rela');//extrae todos los numero de documentos relacionados
								$dataDocRela = $this->_modelComprobantePSE->getComprobatesByNumDocs( $arrayNumDoc, $resultArraCab['datos_emisor']['ruc_emisor'], ["'A'", "'O'"] );
								if ( count($dataDocRela) ) 
								{
									$modelDocumentosReferenciados->save( $dataDocRela ,$resultArraCab, $comprobanteId , 'ANT' );
								}else
								{
									$modelDocumentosReferenciados->saveXml( $resultArrayDocRela ,$resultArraCab, $comprobanteId , 'ANT' );
								}
							}
							$resultSaveCab = $modelFacturaCab->saveCabXml( $resultArraCab );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura';
							$numId = $resultSaveCab['last_val'];
						
							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$resultTributo = $classXmlComprobante->setDataTributo(  $numId );

							$modelFacturaCab->saveDetXml( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura detalle';
							$modelFacturaCab->saveNotaXml( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura nota';
							$modelFacturaCab->saveTributoXml( $resultTributo );
							$message[ 'resgistro_registro_4' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura tributo';
						break;
					case '07':// NOTA DE CREDITO
							$model = new Model_Nota_Credito_CPE();
							$resultArrayDocRela = $classXmlComprobante->setDataDocRela();
							$resultArrayDocAfecta = $classXmlComprobante->setDocumentoAfecta();
							if ( count( $resultArrayDocRela ) )
							{
								$arrayNumDoc = array_column($resultArrayDocRela, 'num_doc_rela');//extrae todos los numero de documentos relacionados
								$dataDocRela = $this->_modelComprobantePSE->getComprobatesByNumDocs( $arrayNumDoc, $resultArraCab['datos_emisor']['ruc_emisor'], ["'A'", "'O'"] );
								if ( count($dataDocRela) ) 
								{
									$modelDocumentosReferenciados->save( $dataDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}else
								{
									$modelDocumentosReferenciados->saveXml( $resultArrayDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}
							}
							$resultSaveCab = $model->saveCabXmlNC( $resultArraCab, $resultArrayDocAfecta );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura';
							$numId = $resultSaveCab['last_val'];
							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$resultTributo = $classXmlComprobante->setDataTributo(  $numId );

							$model->saveDetXml( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura detalle';
							$model->saveNotaXml( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura nota';
							$model->saveTributoXml( $resultTributo );
							$message[ 'resgistro_registro_4' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura tributo';
						break;
					case '08'://NOTA DE DEBITO
							$model = new Model_Nota_Debito_CPE();
							$resultArrayDocRela = $classXmlComprobante->setDataDocRela();
							$resultArrayDocAfecta = $classXmlComprobante->setDocumentoAfecta();
							if ( count( $resultArrayDocRela ) )
							{
								$arrayNumDoc = array_column($resultArrayDocRela, 'num_doc_rela');//extrae todos los numero de documentos relacionados
								$dataDocRela = $this->_modelComprobantePSE->getComprobatesByNumDocs( $arrayNumDoc, $resultArraCab['datos_emisor']['ruc_emisor'], ["'A'", "'O'"] );
								if ( count($dataDocRela) ) 
								{
									$modelDocumentosReferenciados->save( $dataDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}else
								{
									$modelDocumentosReferenciados->saveXml( $resultArrayDocRela ,$resultArraCab, $comprobanteId , 'REF' );
								}
							}
							$resultSaveCab = $model->saveCabXmlND( $resultArraCab, $resultArrayDocAfecta );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura';
							$numId = $resultSaveCab['last_val'];

							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$resultTributo = $classXmlComprobante->setDataTributo(  $numId );

							$model->saveDetXml( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura detalle';
							$model->saveNotaXml( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura nota';
							$model->saveTributoXml( $resultTributo );
							$message[ 'resgistro_registro_4' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla factura tributo';
						break;
					case '20'://RETENCION
							$model = new Model_Retencion_CPE();
							$resultSaveCab = $model->saveCabXmlRET( $resultArraCab );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla RETENCION CABECERA';
							$numId = $resultSaveCab['last_val'];
							
							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$model->saveDetXmlRET( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla RETENCION detalle';
							$model->saveNotaXmlRET( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla RETENCION nota';

						break;
					case '40'://PERCEPCION
							$model = new Model_Percepcion_CPE();
							$resultSaveCab = $model->saveCabXmlPER( $resultArraCab );
							$message[ 'resgistro_registro_1' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla PERCPECION CABECERA';
							$numId = $resultSaveCab['last_val'];
							
							$resultDet = $classXmlComprobante->setDataDetalle( $numId );
							$resultNota = $classXmlComprobante->setDataNota(  $numId);
							$model->saveDetXmlPER( $resultDet );
							$message[ 'resgistro_registro_2' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla PERCPECION detalle';
							$model->saveNotaXmlPER( $resultNota );
							$message[ 'resgistro_registro_3' ] = date('Y-m-d H:m:s:').round(microtime(true) * 1000).' : Se registro correctamente en la tabla PERCPECION nota';
						break;
					
				}
		} catch (Exception $e) 
		{
			$mensaje['error_de_guardado'] = $e->getMessage();
		}
		
		
		
		return $message;
	}
	/**
	*
	*/
	private function sendEmailCPE( $jsonEntrada,$base64PDF, $pathXMLENVIO,$pathFileCDR, $responseService )
	{
		try {
			$fechaEmision =  substr($jsonEntrada->fechaEmision, 0, 10 ) ;
			$config = get_config();
			$ruc_adquiriente_encrypt = openssl_encrypt( $jsonEntrada->oAdquiriente->numeroDocumentoIdentidad , $config[ 'openssl_method_ciphering' ], $config[ 'decryption_key' ], $config[ 'openssl_option' ], $config[ 'openssl_iv' ] );
			$recordEmail =
	    	[
	    		'ruc' =>  $this->_ruc,
	    		'serie' => $this->_serie ,
	    		'numero' => $this->_correlativo ,
	    		'tipo_cpe' =>  $this->_cpeType ,
	    		'razon_social'  =>  $jsonEntrada->oAdquiriente->apellidosNombresDenominacionRazonSocial ,
	    		'fecha' => $fechaEmision,
	    		'token_registro' => $ruc_adquiriente_encrypt,
	    		'enlace_front_sesion_adquiriente' => true,
	    		'enlace_front_registro_adquiriente' => true,
	    		'to_email' => $jsonEntrada->correo
	    	
	    	];
	    	$file_cpe = [];
	    	if ( $responseService !== 'R' ) 
	    	{
	    		$fileAnexo =  get_anexo_by_ticket(  $this->_ruc,  $jsonEntrada->nroTicket );
	    		$file_cpe = [
	    			['file_name' => $this->getFileName() , 'path_file' =>  $pathFileCDR , 'formato' => 'zip'], 
	    			['file_name' => str_replace('zip', 'pdf', $this->getFileName() ) , 'base64' =>  $base64PDF  ,'formato'=> 'pdf' ], 
	    			['file_name' => str_replace('zip', 'xml', $this->getFileName() ) , 'path_file' => $pathXMLENVIO  , 'formato' => 'xml']  
	    		];
	    		if ( count( $fileAnexo ) > 0) 
			    {
			    	if ( $fileAnexo['status'] && !empty(  trim( $fileAnexo['file'] )  ) && !empty(  $fileAnexo['name_file']  ) )  
			    	{
						$fileElement = [ 'file_name' => $fileAnexo['name_file'], 'base64' =>  $fileAnexo['file'] ];
			    		array_push($file_cpe , $fileElement );
			    	}
			    }
	    	}
	    	$modelCorreoPlantilla_PSE = new Model_Correo_Plantilla_PSE();
	    	$recordEmail[ 'tipo_correo' ] = $responseService == 'R' ? 4 : 2;
	    	$template = $modelCorreoPlantilla_PSE->get_email_template( $recordEmail );
	    	$bodyHtml = $template[ 'message' ];
	    	$subject = $template[ 'subject' ];
	    	$toEmail = $jsonEntrada->correo . ','. $template[ 'con_copia' ];
	    	$sender = get_subjet_by_ruc( $this->_ruc );
	    	// 
	    	$message[ 'start' ] = "Iniciando envio email: ". date('Y-m-d H:m:s.').round(microtime(true) * 1000);
	    	$providerServiceEmail = $this->serviceEmailContext->instaceService( $toEmail, $sender, $subject, $file_cpe , $bodyHtml  );
	    	// $providerServiceEmail = $this->serviceEmailContext->instaceService(   );
	    
	    	$responseEmail = $providerServiceEmail->sendEmail();
	    	$message[ 'end' ] = "Finalizo envio email: ". date('Y-m-d H:m:s.').round(microtime(true) * 1000);
	    	$message[ 'response' ] = $responseEmail;
	    	$message[ 'record' ] = $recordEmail;
	    	return [ 'response' => $responseEmail, 'message' => $message ];

		} catch (Exception $e) 
		{
			$message[ 'status' ] = false;
			$message[ 'response' ] = $e->getMessage();
			return $message;
		}
		
	}
	/**
	* cuando el servicio devuelve ciertos estados que son para reprocesar, entonces se prepara el comprobante para volver a enviarse 
	*/
	private function sinprocesar() 
	{
		try {
			$dataJob = [
			'estado' => NULL,
			];
			$whereJob =[
				'tb_comprobante_id' => $this->_comprobanteId
			];
			$this->_modelComprobantePSE->CI_UPDATE( $dataJob, $whereJob, 'job_info' );
		} catch (Exception $e ) {
			write_log( $e->getMessage(). " en la funcion sinprocesar() idComprobante : ". $idComprobante  );
		}
			
	}
	private function getMontoByTypeMoneda( $data )
	{
		switch ( $this->_cpeType ) {
			case '20':
					$montoTotal = $data->montoTotalRetenido;
				break;
			case '40':
					$montoTotal = $data->montoTotalPercibido;
				break;
			default:
					$montoTotal = $data->montoTotalMonedaOriginal;
				break;
		}
		return $montoTotal;
	}
}