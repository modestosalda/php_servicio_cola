<?php 
namespace Helpers;
/**
 * Se encarga de parsear la informacion del xml a array para los comprobante persepcion y retencion
 */
error_reporting(E_ERROR | E_PARSE);
class XML_ComprobantePerRet
{
	private $xmlObj ;
	private $tipoCpeDoc;
	function __construct($ruta = '', $tipoCpe = '') 
	{
		if ( $ruta != '' ) 
		{
			$file = file_get_contents( $ruta);
			$this->xmlObj =  simplexml_load_string($file);	
			
		}
		$this->tipoCpeDoc = $tipoCpe;
	}
	/**
	* seta la data del xml y retorna un array
	*/
	function setDataXml()
	{
		$tipoCpe = $this->tipoCpeDoc;
		$xml =  $this->xmlObj;

		######################################     DATOS EMISOR  ###################################################
		$ruc_emisor = end($xml->xpath('cac:AgentParty/cac:PartyIdentification/cbc:ID')[0]);
		$tipodocumentoemisor = $xml->xpath('cac:AgentParty/cac:PartyIdentification/cbc:ID')[0]['schemeID']; # ?


		######################################     DATOS ADQUIRIENTE  ##############################################
		$tipodocumentocliente =$xml->xpath('cac:AgentParty/cac:PartyIdentification/cbc:ID')[0]['schemeID'];
		$numeroDocumentoIdentidad = $xml->xpath('cac:AgentParty/cac:PartyIdentification/cbc:ID')[0];
		$clie_razonsocial =end($xml->xpath('cac:AgentParty/cac:PartyName/cbc:Name')[0]); 
		$clie_ubigeo = isset($xml->xpath('cac:AgentParty/cac:PostalAddress/cbc:ID')[0]) ? end($xml->xpath('cac:AgentParty/cac:PostalAddress/cbc:ID')[0]) : '';
		$urbanizacion =isset($xml->xpath('cac:AgentParty/cac:PostalAddress/cbc:StreetName')[0]) ? end($xml->xpath('cac:AgentParty/cac:PostalAddress/cbc:StreetName')[0]) : '';
		$distrito = isset($xml->xpath('cac:AgentParty/cac:PostalAddress/cbc:District')[0]) ?end($xml->xpath('cac:AgentParty/cac:PostalAddress/cbc:District')[0]) : "";
		$provincia = isset($xml->xpath('cac:AgentParty/cac:PostalAddress/cbc:CityName')[0]) ?end($xml->xpath('cac:AgentParty/cac:PostalAddress/cbc:CityName')[0]) : '';
		$cod_pais = isset($xml->xpath('cac:AgentParty/cac:PostalAddress/cac:Country/cbc:IdentificationCode')[0]) ? end($xml->xpath('cac:AgentParty/cac:PostalAddress/cac:Country/cbc:IdentificationCode')[0]) : '';
		$departamento = isset($xml->xpath('cac:AgentParty/cac:PostalAddress/cbc:CityName')[0]) ? end($xml->xpath('cac:AgentParty/cac:PostalAddress/cbc:CityName')[0]) : '';
		$dir_completa =  $clie_ubigeo.' '.$urbanizacion.' '.$distrito.' '.$provincia.' '.$cod_pais;


		######################################     DETALLES CPE  ###################################################
		$numeroCorrelativo = substr($xml->xpath("cbc:ID")[0], 5, 13);
		$numeroSerie = substr($xml->xpath("cbc:ID")[0], 0, 4);
		$fecha = $xml->xpath("cbc:IssueDate")[0];
		$hora = $xml->xpath("cbc:IssueTime")[0];
		$tipoCpeString = $tipoCpe == '20' ? 'Retention' : 'Perception';
		$cpe_reg = end($xml->xpath("sac:SUNAT".$tipoCpeString."SystemCode")[0]) ;
		
		###############################     DATOS GLOBALES IMPUESTOS, BASE ,TOTALES  ###############################

		$mnt_per = floatval(end($xml->xpath("cbc:TotalInvoiceAmount")[0]));
		// var_dump($mnt_per);
		// exit;
		$mnt_cob_per = $tipoCpe=='20'? floatval(end($xml->xpath("sac:SUNATTotalPaid")[0])) : floatval(end($xml->xpath("sac:SUNATTotalCashed")[0]));
		$codigoTipoMoneda  = NULL;
		foreach ( $xml->xpath("sac:SUNAT".$tipoCpeString."DocumentReference" ) as $val) 
		{
			$codigoTipoMoneda =strval( end($val->xpath("cac:Payment/cbc:PaidAmount")[0]['currencyID']) );
		}
		
		$data=
		[
			'datos_emisor' => [
				'ruc_emisor' => strval( $ruc_emisor),
				'tipodocumentoemisor' =>  strval( $tipodocumentoemisor)
			],
			'datos_adquiriente' =>
			[
				'tipodocumentocliente' =>strval( $tipodocumentocliente),
				'numeroDocumentoIdentidad' => strval($numeroDocumentoIdentidad),
				'clie_razonsocial' => strval($clie_razonsocial),
				'clie_ubigeo' => strval($clie_ubigeo),
				'urbanizacion' => strval($urbanizacion),
				'distrito' => strval($distrito),
				'provincia' => strval($provincia),
				'cod_pais' => strval($cod_pais),
				'departamento' => strval($departamento),
				'dir_completa' => strval($dir_completa),
			],
			'detalle_cpe' => [
				'numeroCorrelativo' => strval($numeroCorrelativo),
				'numeroSerie' => strval($numeroSerie),
				'fecha' => strval($fecha),
				'hora' => strval($hora),
				'cpe_reg' => strval($cpe_reg),
				// 'codigoTipoOperacion' => strval($codigoTipoOperacion),
				// 'codigoTipoMoneda' => strval($codigoTipoMoneda),
				// 'codigoTipoDocumento' =>strval( $codigoTipoDocumento),
			],
			'datos_globales' => [
				'mnt_per' => $mnt_per,
				'mnt_cob_per' => $mnt_cob_per,
				'cpe_cod_mone' => $codigoTipoMoneda,
			],
		];
		return $data;
	}

	function setDataDetalle(  $num_id )
	{
		$data = [];
		$tipoCpe = $this->tipoCpeDoc;
		$xml =  $this->xmlObj;
		
		$nroOrden = 1;
		$tipoCpeString = $tipoCpe == '20' ? 'Retention' : 'Perception';
		$is_paid_or_cached = $tipoCpe == '20' ? 'Paid' : 'Cashed'; 
		foreach ($xml->xpath("sac:SUNAT".$tipoCpeString."DocumentReference") as $val) 
		{
			// OBTIENE UNIDAD DE MEDIDA DEL ITEM			
			$codigoTipoMoneda = end($val->xpath("cac:Payment/cbc:PaidAmount")[0]['currencyID']);
			// num_id	# cab (por cada detalle se genera un tag 
			$rela_num_tipo = end($val->xpath("cbc:ID")[0] ['schemeID']);
			$rela_num_arr = explode('-', end($val->xpath("cbc:ID")[0]));
			$rela_num_serie = $rela_num_arr[0];
			$rela_num_correl = $rela_num_arr[1];
			$rela_fecha = 	end($val->xpath("cbc:IssueDate")[0]);
			$rela_cod_mone =	end($val->xpath("cbc:TotalInvoiceAmount")[0] ['currencyID']);
			$rela_importe_total =	floatval(end($val->xpath("cbc:TotalInvoiceAmount")[0]));
			$fecha_pago = 	end($val->xpath("cac:Payment/cbc:PaidDate")[0])." 00:00:00";
			$num_pago =	end($val->xpath("cac:Payment/cbc:ID")[0]);
			$importe_sin_ret =	floatval(end($val->xpath("cac:Payment/cbc:PaidAmount")[0])); 
			$cod_mone_pago =	end($val->xpath("cac:Payment/cbc:PaidAmount")[0]['currencyID']);
			$importe_ret =	floatval(end($val->xpath("sac:SUNAT".$tipoCpeString."Information/sac:SUNAT".$tipoCpeString."Amount")[0]));
			$fecha_ret	= end($val->xpath("sac:SUNAT".$tipoCpeString."Information/sac:SUNAT".$tipoCpeString."Date")[0])." 00:00:00";
			$total_pagar_neto	= floatval(end($val->xpath("sac:SUNAT".$tipoCpeString."Information/sac:SUNATNetTotal".$is_paid_or_cached)[0]));
			$cod_mone_neto	= end($val->xpath("sac:SUNAT".$tipoCpeString."Information/sac:SUNATNetTotal".$is_paid_or_cached)[0]['currencyID']);
			$mone_ref_tc = ($val->xpath("sac:SUNAT".$tipoCpeString."Information/cac:ExchangeRate/cbc:SourceCurrencyCode")[0])? end($val->xpath("sac:SUNAT".$tipoCpeString."Information/cac:ExchangeRate/cbc:SourceCurrencyCode")[0]): "";
			$mone_obj_tc = ( $val->xpath("sac:SUNAT".$tipoCpeString."Information/cac:ExchangeRate/cbc:TargetCurrencyCode") )? end($val->xpath("sac:SUNAT".$tipoCpeString."Information/cac:ExchangeRate/cbc:TargetCurrencyCode") )  : "";	
			$factor_mone_origen =	isset( $val->xpath("sac:SUNAT".$tipoCpeString."Information/cac:ExchangeRate/cbc:CalculationRate")[0] )? end($val->xpath("sac:SUNAT".$tipoCpeString."Information/cac:ExchangeRate/cbc:CalculationRate"))  : "";	
			$fecha_tc = isset( $val->xpath("sac:SUNAT".$tipoCpeString."Information/cac:ExchangeRate/cbc:Date")[0])? end($val->xpath("sac:SUNAT".$tipoCpeString."Information/cac:ExchangeRate/cbc:Date")[0])." 00:00:00" : NULL;
			$cod_mone_imp_ret	= end($val->xpath("sac:SUNAT".$tipoCpeString."Information/sac:SUNATNetTotal".$is_paid_or_cached)[0]['currencyID']);
			$is_cob_or_pag = $tipoCpe == '20'? 'pago':'cobro';
			$is_per_or_ret = $tipoCpe == '20'? 'ret':'per';
			$arrayData = 
			[
				'num_id' => $num_id,
				'rela_num_tipo' => $rela_num_tipo,
				'rela_num_serie' => $rela_num_serie,
				'rela_num_correl' => $rela_num_correl ,
				'rela_fecha' => $rela_fecha ,
				'rela_cod_mone' => $rela_cod_mone,
				'rela_importe_total' => $rela_importe_total,
				'cod_mone_neto' => $cod_mone_neto,
				'mone_obj_tc' => strval( $mone_obj_tc),
				'mone_ref_tc' => $mone_ref_tc,

				'fecha_' . $is_cob_or_pag => $fecha_pago,
				'num_' . $is_cob_or_pag => $num_pago,
				'cod_mone_' . $is_cob_or_pag => $cod_mone_pago,
				'importe_sin_' . $is_per_or_ret => $importe_sin_ret,
				'importe_' . $is_per_or_ret => $importe_ret,
				'fecha_' . $is_per_or_ret => $fecha_ret,
				
				'factor_mone_origen' => strval(  $factor_mone_origen)
			];
			if ( $tipoCpe=='20' ) 
				$arrayData[ "total_pagar_neto" ] = $total_pagar_neto; 
			else
				$arrayData[ "total_cobrar_neto" ] = $total_pagar_neto; 

			if ( $fecha_tc != NULL ) 
			{
				$arrayData[ "fecha_tc" ] = $fecha_tc;
			}
			array_push($data, $arrayData);
		}//endforeach final
		return $data;
	}

	function setDataNota( $num_id )
	{
		$xml =  $this->xmlObj;
		$data = [];
		foreach ($xml->xpath("ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation") as $nodoNote )
		{
   			$descripcion_nota = isset($nodoNote->xpath("sac:AdditionalProperty/cbc:Value")[0])  ? end($nodoNote->xpath("sac:AdditionalProperty/cbc:Value")[0]): '';
   			$codigo_nota = end($nodoNote->xpath("sac:AdditionalProperty/cbc:ID")[0]);
   			$arrayData = 
   			[
   				'num_id' => $num_id,
   				'descripcion' => $descripcion_nota,
   				'codigo' => $codigo_nota
   			];
   			array_push($data, $arrayData);
		}
		return $data;
	}
	
}

 ?>