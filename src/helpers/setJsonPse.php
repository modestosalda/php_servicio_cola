<?php 

/**
* setea el array de Aquiriente
*/
if ( !function_exists('setOAquierente') ) 
{
     function setOAquierente(Array $data )
    {
        $data =  $data[ 0 ];
        $descripcionTipoDocumentoIdentidad = $data[ "clie_tip_doc" ] == '1' ? 'DNI' : 'RUC';
        $array = [
            'apellidosNombresDenominacionRazonSocial' => $data[ 'clie_rz_social' ],
            'descripcionTipoDocumentoIdentidad' =>  $descripcionTipoDocumentoIdentidad,
            'codigoTipoDocumentoIdentidad' => $data[ 'clie_tip_doc' ],
            'numeroDocumentoIdentidad' => $data[ 'cpe_num_ruc' ]
        ];
        return $array;
    }
}


if ( !function_exists('lComprobanteModificado') ) 
{
    /**
    *data 
    * tipo cpe es el tipo de comprobante
    * $codigoTipoNota es codigo o tipo de generacion de nota de credito
    **/
     function lComprobanteModificado( $data, $tipoCpe, $motivo, $codigoTipoNota):Array
    {
        $data =  $data[ 0 ];
        //recorrer en un foir
        $array = [
            'codigoTipoDocumento' => $tipoCpe,
            'numeroSerie' =>   $data[ 'cpe_num_serie' ],
            'numeroCorrelativo' => $data[ 'cpe_num_correl' ],
            'codigoTipoNota' => $codigoTipoNota,
            'lMotivoSustento' => [ $motivo ],
        ];

        return $array;
    }
}

if ( !function_exists('setLNota') ) 
{
    /**
    * data 
    **/
     function setLNota( Array $data ):Array
    {
        $data =  $data[ 0 ];
        //recorrer en un foir
        $array[] = [
            'descripcion' => $data[ 'descripcion' ],
            'codigo' =>   $data[ 'codigo' ]
        ];

        return $array;
    }
}

if ( !function_exists('setLImpuesto') ) 
{
    /**
    * data 
    **/
     function setLImpuesto(Array $data ):Array
    {
        $arrayData =[];
        foreach ($data as  $value) 
        {
            array_push($arrayData, 
                [ 
                    'codigoTipoAfectacionIGV' => $value[ 'imp_cod_afec_trib' ] ,  
                    'montoTotalMonedaOriginal' => round( $value[ 'mnt_total_trib' ] ,2) , 
                    'montoBaseMonedaOriginal' => round( $value[ 'mnt_base' ] , 2) ,
                    'nombre' =>  $value[ 'imp_nombre' ]  ,
                    'codigoInternacional' =>  $value[ 'imp_cod_inter' ]  ,
                    'codigo' =>  $value[ 'imp_cod_trib' ]  ,
                    'porcentaje' =>  $value[ 'imp_porc' ]  
                ] );
        }
        return $arrayData;
    }
}


if ( !function_exists('setLDetalleNotaCredito') ) 
{
    /**
    * data 
    **/
     function setLDetalleNotaCredito( $data )
    {
        $arrayData = [];
    
        foreach ($data as $key =>  $value) 
        {

            array_push( $arrayData , 
                [ 
                    'numeroOrden'  => $key + 1 ,
                    'cantidad'  => $value[ 'item_cant' ] ,
                    'montoValorVentaUnitarioMonedaOriginal'  => $value[ 'item_mnt_unt' ] ,
                    'montoPrecioVentaUnitarioMonedaOriginal'  => $value[ 'item_pu' ] ,
                    'montoValorVentaTotalMonedaOriginal'  => $value[ 'imp_mnt_base' ] ,
                    'montoTotalMonedaOriginal'  => $value[ 'item_mnt_valor_total' ] ,
                    'oProducto' => [
                        "codigoUnidadMedida" => $value[ 'item_und' ] ,
                        "descripcion" => $value[ 'item_des' ] ,
                        // "codigoSunat" => $value[ 'item_cod_sunat' ] ,
                        "codigo" => $value[ 'item_cod' ] ,
                    ],
                    'lImpuesto' => [
                        [ 
                            'codigoTipoAfectacionIGV'  => $value[ 'imp_cod_afec_trib' ],
                            'montoTotalMonedaOriginal'  => $value[ 'imp_mnt_total_trib' ],
                            'montoBaseMonedaOriginal'  => $value[ 'imp_mnt_base' ],
                            'nombre'  => $value[ 'imp_nombre' ],
                            'codigoInternacional'  => $value[ 'imp_cod_inter' ],
                            'codigo'  => $value[ 'imp_cod_trib' ],
                            'porcentaje'  => $value[ 'imp_porc' ],
                        ]
                    ]
                ] 
            );
            if ($value[ 'item_cod_sunat' ] != NULL || $value[ 'item_cod_sunat' ] !="") 
            {
                
                $arrayData[ $key ]['oProducto'][ 'codigoSunat' ] = $value[ 'item_cod_sunat' ] ;
            }
        }
       
        return $arrayData;
    }
}