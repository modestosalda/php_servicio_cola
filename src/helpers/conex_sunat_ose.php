<?php 
/**
* Escribe un mensaje de error 
* @param $usuario_sol del remitente
* @param $pass_sol contraseña del remitente
*/
if (  ! function_exists( 'hppt_request_sendsummary_soap' )  ) 
{
	function hppt_request_sendsummary_soap( $usuario_sol , $pass_sol, $nombre_cpe, $base64ZIP , $ruta_ws,$tipoDocumento = '' ,$tipoOperador = 0)
    {
    	require $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/helpers/common_helper.php';
    	$xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
							  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe"
							  xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
							  <soapenv:Header>
							  <wsse:Security>
							  <wsse:UsernameToken>
							  <wsse:Username>' . $usuario_sol . '</wsse:Username>
							  <wsse:Password>' . $pass_sol . '</wsse:Password>
							  </wsse:UsernameToken>
							  </wsse:Security>
							  </soapenv:Header>
							  <soapenv:Body>
							  <ser:sendSummary>
							  <fileName>' . $nombre_cpe . '</fileName>
							  <contentFile>' . $base64ZIP . '</contentFile>
							  </ser:sendSummary>
							  </soapenv:Body>
							  </soapenv:Envelope>';
							    $headers = array(
							                        "Content-Type: text/xml;charset=UTF-8",
							                        "Cache-Control: no-cache",
							                        'SOAPAction: "urn:sendSummary"',
							                        // "Host: test.conose.pe",
							                        "Connection: Keep-Alive",
							                        "Content-length: ".strlen($xml_post_string),
							                    ); //SOAPAction: your op URL

							    $ch = curl_init();
							    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
							    curl_setopt($ch, CURLOPT_URL, trim( $ruta_ws));
							    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

							    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
							    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
							    curl_setopt($ch, CURLOPT_POST, true);
							    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
							    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
							    $response = curl_exec($ch);
							    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
							    curl_close($ch);
		if ( !$response ) 
		{
			$error = "HPPT REQUEST ERROR CURL: error de conexion al WS SOAP con URL: " . $ruta_ws . ", TIPO DOCUMENTO: " . $tipoDocumento . " CURL MESSAGE_ERROR: " . curl_error($ch) . " PARAMETROS ENVIADOS : "  . $xml_post_string ;
			throw new Exception( $error, 1);
			
		}else
		{
			$mensaje = "HTTP REQUEST SUCCESSFUL: conexion exitosa al WS SOAP con URL: " . $ruta_ws . ",  TIPO DOCUMENTO: " . $tipoDocumento . "  PARAMETROS ENVIADOS : "  . $xml_post_string . ", CON RESPUESTA :" . $response;
			write_log( $mensaje, 1 );
			echo $mensaje. "<br>";
			return [ 'response' => $response , 'httpcode' => $httpcode ];
		}


      	
    }

}

/**
* Escribe un mensaje de error 
* @param $usuario_sol del remitente
* @param $pass_sol contraseña del remitente
*/
if (  ! function_exists( 'hppt_request_sendsummary_soap_getStatus' )  ) 
{
	function hppt_request_sendsummary_soap_getStatus( $usuario , $clave, $tb_comprobante_ticket ,$ruta_ws,$tipoDocumento,   $tipoOperador = 0)
    {
    	// require $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/helpers/common_helper.php';

    	
        switch ( $tipoOperador ) 
        {
         	case '1'://OSE CON OSE
					$servicio = 'OSECONOSE';
		         	$xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
		                    <soapenv:Header>
		                        <wsse:Security>
		                            <wsse:UsernameToken>
		                                <wsse:Username>' . $usuario . '</wsse:Username>
		                                <wsse:Password>' . $clave . '</wsse:Password>
		                            </wsse:UsernameToken>
		                        </wsse:Security>
		                    </soapenv:Header>
		                    <soapenv:Body>
		                      <ser:getStatus>
		                        <ticket>' . $tb_comprobante_ticket . '</ticket>
		                      </ser:getStatus>
		                    </soapenv:Body>
		                </soapenv:Envelope>';
         			$headers = array(
				        "Content-type: text/xml;charset=\"utf-8\"",
				        "Accept: text/xml",
				        "Cache-Control: no-cache",
				        "Pragma: no-cache",
				        "SOAPAction:  urn:getStatus",
				        "Content-length: " . strlen($xml_post_string),
			   		 );
         		break;
			case '0': //SUNAT
				$servicio = 'SUNAT';
				$headers = [];
				$xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
						<soapenv:Header>
							<wsse:Security>
								<wsse:UsernameToken>
									<wsse:Username>' . $usuario . '</wsse:Username>
									<wsse:Password>' . $clave . '</wsse:Password>
								</wsse:UsernameToken>
							</wsse:Security>
						</soapenv:Header>
						<soapenv:Body>
						  <ser:getStatus>
							<ticket>' . $tb_comprobante_ticket . '</ticket>
						  </ser:getStatus>
						</soapenv:Body>
					</soapenv:Envelope>';
			break;
			default :
				$servicio = 'OSE';
				$xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
						<soapenv:Header>
							<wsse:Security>
								<wsse:UsernameToken>
									<wsse:Username>' . $usuario . '</wsse:Username>
									<wsse:Password>' . $clave . '</wsse:Password>
								</wsse:UsernameToken>
							</wsse:Security>
						</soapenv:Header>
						<soapenv:Body>
						  <ser:getStatus>
							<ticket>' . $tb_comprobante_ticket . '</ticket>
						  </ser:getStatus>
						</soapenv:Body>
					</soapenv:Envelope>';
				$headers = array(
				        "Content-type: text/xml;charset=\"utf-8\"",
				        "Accept: text/xml",
				        "Cache-Control: no-cache",
				        "Pragma: no-cache",
				        "SOAPAction:  urn:getStatus",
				        "Content-length: " . strlen($xml_post_string),
			   		 );
			break;         	
        }
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_URL, trim($ruta_ws));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
	    if ( count( $headers ) ) 
	    {
	    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);	
	    }
	    $response = curl_exec($ch);
	    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	    curl_close($ch);
    	if ( !$response ) 
		{
			$error = "HPPT REQUEST ERROR CURL: error de conexion al WS SOAP con URL: " . $ruta_ws . ", TIPO DOCUMENTO: " . $tipoDocumento . " CURL MESSAGE_ERROR: " . curl_error($ch) . " PARAMETROS ENVIADOS : "  . $xml_post_string . " servicio: ". $servicio;
			throw new Exception( $error, 1);
			
		}else
		{
			$mensaje = "HTTP REQUEST SUCCESSFUL: conexion exitosa al WS SOAP con URL: " . $ruta_ws . ",  TIPO DOCUMENTO: " . $tipoDocumento . "  PARAMETROS ENVIADOS : "  . $xml_post_string . ", CON RESPUESTA :" . $response . " servicio: ". $servicio;
			write_log( $mensaje, 1 );
			$response = 
			[
				'URL_SERVICIO' => $ruta_ws ,
				'SERVICIO' => $servicio ,
				'TIPO_DOCUMENTO' => $tipoDocumento ,
				'HTTP_CODE' => $httpcode ,
				'XML_ENVIO' => $xml_post_string ,
				'XML_RESPUESTA' => $response 
			];
			return $response;
		}	
    }

}


/**
* Escribe un mensaje de error 
* @param $usuario_sol del remitente
* @param $pass_sol contraseña del remitente
*/
if (  ! function_exists( 'hppt_request_soap_sendBill' )  ) 
{
	function hppt_request_soap_sendBill( $usuario , $pass, $ruta_ws,$tipoDocumento, $nombre_file, $base64ZIP ,  $tipoOperador = 0)
    {
    	// require $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/helpers/common_helper.php';

    	
        switch ( $tipoOperador ) 
        {

         	case '1'://OSE CON OSE
					$servicio = 'OSECONOSE';
		         	$xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
										  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe"
										  xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
										  <soapenv:Header>
											  <wsse:Security>
												  <wsse:UsernameToken>
													  <wsse:Username>' . $usuario . '</wsse:Username>
													  <wsse:Password>' . $pass . '</wsse:Password>
												  </wsse:UsernameToken>
											  </wsse:Security>
										  </soapenv:Header>
										  <soapenv:Body>
											  <ser:sendBill>
												  <fileName>' . $nombre_file . '</fileName>
												  <contentFile>' . $base64ZIP . '</contentFile>
											  </ser:sendBill>
										  </soapenv:Body>
										  </soapenv:Envelope>';
         			$headers =  [
							        "Content-type: text/xml;charset=\"utf-8\"",
							        "Accept: text/xml",
							        "Cache-Control: no-cache",
							        "Pragma: no-cache",
							        "SOAPAction:  urn:sendBill",
						        	"Content-length: " . strlen($xml_post_string),
						    	];
         		break;
			case '0': //SUNAT
				$servicio = 'SUNAT';
				$xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
									  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe"
									  xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
										  <soapenv:Header>
											  <wsse:Security>
											  <wsse:UsernameToken>
												  <wsse:Username>' . $usuario . '</wsse:Username>
												  <wsse:Password>' . $pass . '</wsse:Password>
												  </wsse:UsernameToken>
											  </wsse:Security>
										  </soapenv:Header>
										  <soapenv:Body>
											  <ser:sendBill>
												  <fileName>' . $nombre_file . '</fileName>
												  <contentFile>' . $base64ZIP . '</contentFile>
											  </ser:sendBill>
										  </soapenv:Body>
									</soapenv:Envelope>';
				$headers =  [
						        "Content-type: text/xml;charset=\"utf-8\"",
						        "Accept: text/xml",
						        "Cache-Control: no-cache",
						        "Pragma: no-cache",
						        "SOAPAction:  urn:sendBill",
						        "Content-length: " . strlen($xml_post_string),
						    ];
			break;
			default :
				$servicio = 'OSE';
				$xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
										  xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe"
										  xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
										  <soapenv:Header>
											  <wsse:Security>
												  <wsse:UsernameToken>
													  <wsse:Username>' . $usuario . '</wsse:Username>
													  <wsse:Password>' . $pass . '</wsse:Password>
												  </wsse:UsernameToken>
											  </wsse:Security>
										  </soapenv:Header>
										  <soapenv:Body>
											  <ser:sendBill>
												  <fileName>' . $nombre_file . '</fileName>
												  <contentFile>' . $base64ZIP . '</contentFile>
											  </ser:sendBill>
										  </soapenv:Body>
										  </soapenv:Envelope>';
         			$headers =  [
							        "Content-type: text/xml;charset=\"utf-8\"",
							        "Accept: text/xml",
							        "Cache-Control: no-cache",
							        "Pragma: no-cache",
							        "SOAPAction:  urn:sendBill",
						        	"Content-length: " . strlen($xml_post_string),
						    	];
			break;         	
        }
        $entradaDocumento = 'Entrada de documento a : '. $servicio . ' nombre de archivo :'. $nombre_file. ' fecha y hora:' . date( 'Y-m-d H:i:s:v' ) ;
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_URL, trim($ruta_ws));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
	    if ( count( $headers ) ) 
	    {
	    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);	
	    }
	    $response = curl_exec($ch);
	    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $salidaDocumento = 'Respuesta de documento a : '. $servicio . ' nombre de archivo :'. $nombre_file. ' fecha y hora:' . date( 'Y-m-d H:i:s:v' );
	    curl_close($ch);
    	if ( !$response ) 
		{
			$error = "HPPT REQUEST ERROR CURL: error de conexion al WS SOAP con URL: " . $ruta_ws . ", TIPO DOCUMENTO: " . $tipoDocumento . " CURL MESSAGE_ERROR: " . curl_error($ch) . "\n PARAMETROS ENVIADOS : "  . $xml_post_string . " \n servicio: ". $servicio . " , HTTPCODE :".  $httpcode;
			throw new Exception( $error, 1);
			
		}else
		{
			
			$mensaje = "HTTP REQUEST SUCCESSFUL: conexion exitosa al WS SOAP con URL: " . $ruta_ws . ",  TIPO DOCUMENTO: " . $tipoDocumento . " \n PARAMETROS ENVIADOS : "  . $xml_post_string . ",  \n CON RESPUESTA :" . $response . " \n servicio: ". $servicio;
			write_log( $mensaje, 1 );
			$response = 
			[
				'START_CURL' => $entradaDocumento ,
				'END_CURL' => $salidaDocumento ,
				'URL_SERVICIO' => $ruta_ws ,
				'SERVICIO' => $servicio ,
				'TIPO_DOCUMENTO' => $tipoDocumento ,
				'HTTP_CODE' => $httpcode ,
				'XML_ENVIO' => $xml_post_string ,
				'XML_RESPUESTA' => $response 
			];
			return $response;
		}	
    }

}


function http_request_get_status( $ruta_ws,  $user_sol, $clave_sol, $tb_comprobante_ruc, $tipo_cpe, $serie, $correlativo, $tipo_operador)
{
	// echo $serie;
	// exit;
	// $user_sol = '20517182673XERO';
	// $clave_sol = 'B4JU6NO3IS';
	$servicio= '';
	$xml_post_string = '<soapenv:Envelope 
				xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
				xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe"
				xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
                 >
				<soapenv:Header>
				 <wsse:Security>
				    <wsse:UsernameToken>
				            <wsse:Username>'.$user_sol.'</wsse:Username>
				            <wsse:Password>'.$clave_sol.'</wsse:Password>
				    </wsse:UsernameToken>
				 </wsse:Security>
				</soapenv:Header>
				<soapenv:Body>
					<m:getStatusCdr xmlns:m="http://service.sunat.gob.pe">
					<rucComprobante>'.$tb_comprobante_ruc.'</rucComprobante>
					<tipoComprobante>'.$tipo_cpe.'</tipoComprobante>
					<serieComprobante>'.$serie .'</serieComprobante>
					<numeroComprobante>'.$correlativo .'</numeroComprobante>
				</m:getStatusCdr>
				</soapenv:Body>
			</soapenv:Envelope>';
			// echo $xml_post_string;
			// exit;
	$headers = [];
    if ( $tipo_operador  != 0 ) 
    {
    	$servicio = 'OSE';
    	$headers = 
    	array(
		        "Content-type: text/xml;charset=\"utf-8\"",
		        "Accept: text/xml",
		        "Cache-Control: no-cache",
		        "Pragma: no-cache",
		        "SOAPAction:  urn:getStatusCdr",
		        "Content-length: " . strlen($xml_post_string),
	   		 );
    }else
    {
    	$servicio = 'SUNAT';
    	$headers = array(
		        "Content-type: text/xml;charset=\"utf-8\"",
		        "Accept: text/xml",
		        "Cache-Control: no-cache",
		        "HOST: e-factura.sunat.gob.pe",
		        "Pragma: no-cache",
		        "SOAPAction:  urn:getStatusCdr",
		        "Content-length: " . strlen($xml_post_string),
	   		 );
    }
	$ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, $ruta_ws);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    if ( count( $headers ) ) 
    {
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);	
    }
    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ( !$response ) 
	{
		$error = "HPPT REQUEST ERROR CURL: error de conexion al WS SOAP con URL: " . $ruta_ws . ", TIPO DOCUMENTO: " . $tipo_cpe . " CURL MESSAGE_ERROR: " . curl_error($ch) . " PARAMETROS ENVIADOS : "  . $xml_post_string . " servicio: ". $servicio . " httpCode: $httpcode ";
		write_log($error);

		throw new Exception( $error, 1);
		
	}else
	{
		$mensaje = "HTTP REQUEST SUCCESSFUL: conexion exitosa al WS SOAP con URL: " . $ruta_ws . ",  TIPO DOCUMENTO: " . $tipo_cpe . "  PARAMETROS ENVIADOS : "  . $xml_post_string . ", CON RESPUESTA :" . $response . " servicio: ". $servicio;
		write_log( $mensaje, 1 );
		$response = 
		[
			'URL_SERVICIO' => $ruta_ws ,
			'SERVICIO' => $servicio ,
			'TIPO_DOCUMENTO' => $tipo_cpe ,
			'HTTP_CODE' => $httpcode ,
			'XML_ENVIO' => $xml_post_string ,
			'XML_RESPUESTA' => $response 
		];
		return $response;
	}	
}
