<?php header("Access-Control-Allow-Origin: *");
/**
* setea la respuesta de sunat y ose
*/
function setResponseRechazado( $doc )
{
	$mensaje[ 'cod_service' ] = '';
	if ( $doc->getElementsByTagName('faultcode')->item(0)->nodeValue == "s:Client") 
	{
		$mensaje[ 'cod_service' ] = $doc->getElementsByTagName('faultstring')->item(0)->nodeValue;
		foreach ($doc->getElementsByTagName('detail') as $nodo_detail) 
		{	
			$mensaje['msj_service'] = $nodo_detail->getElementsByTagName('message')->item(0)->nodeValue;
		}
	}else 
	{
		$mensaje[ 'cod_service' ] = preg_replace('/[^0-9]+/', '', $doc->getElementsByTagName('faultcode')->item(0)->nodeValue) ;
		$mensaje[ 'msj_service' ] = $doc->getElementsByTagName('faultstring')->item(0)->nodeValue;
	}
	return $mensaje;
}
?>