<?php 
// namespace Modelos;
// require '../Config/autoload.php';
Include_once   $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/Modelos/CI_Model.php';
namespace Models;

// use CI_Model;
/**
 * Modelo Resumen Diario
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */
class Model_Comunicacion_Baja_CPE extends CI_Model
{
	const TABLE_NAME = 'sh_cloud_cpe.tb_comunicacionbaja';
	function __construct()
	{
		parent::__construct(self::TABLE_NAME);	
	}
    
    public function getComunicacionBajaCabAndDetByIdentificador( $identificador, $ruc )
    {
        $query = "SELECT
                        (tb_comunicacionbaja_detalle.tb_comudeta_identif)::text AS identificacion,
                        (tb_comunicacionbaja.tb_comunicacionb_fechemi)::text AS fecha_emision,
                        (tb_comunicacionbaja_detalle.tb_comudeta_nombrecpe)::text AS nombrecpe,
                        (tb_comunicacionbaja_detalle.tb_comudeta_estado)::text AS estado,
                        split_part((tb_comunicacionbaja_detalle.tb_comudeta_nombrecpe)::text, '-'::text, 1) AS ruc,
                        split_part((tb_comunicacionbaja_detalle.tb_comudeta_nombrecpe)::text, '-'::text, 2) AS tipocomprobante,
                        split_part((tb_comunicacionbaja_detalle.tb_comudeta_nombrecpe)::text, '-'::text, 3) AS serie,
                        split_part((tb_comunicacionbaja_detalle.tb_comudeta_nombrecpe)::text, '-'::text, 4) AS numerocomprobante,
                        tb_comudeta_nombrecpe nombrecpe
                    FROM
                        sh_cloud_cpe.tb_comunicacionbaja,
                        sh_cloud_cpe.tb_comunicacionbaja_detalle 
                    WHERE
                        sh_cloud_cpe.tb_comunicacionbaja.tb_comunicacionb_identif = tb_comunicacionbaja_detalle.tb_comudeta_identif 
                        and tb_comudeta_comunicacionb_cod = tb_comunicacionb_cod
                        AND tb_comudeta_identif = '" . $identificador . "'
                        AND tb_comunicacionb_ruc = '" . $ruc . "'
                        ";
        $result = $this->queryPersonalizate( $query );
        return $result;       
    }
    public function getComunicacionBajaCabByIdentificador( $identificador, $ruc = '' )
    {
        $query = "SELECT
                    * 
                FROM
                    sh_cloud_cpe.tb_comunicacionbaja 
                WHERE
                    tb_comunicacionb_identif = '" . $identificador . "'
                    and tb_comunicacionb_ruc = '".$ruc."'
                    ";

        $result = $this->queryPersonalizate( $query );
        return $result;       
    }
    public function saveDataCab(array $data  )
    {
        // $dataSave = [ 'tb_resumend_ruc' => 2121336546 , 'tb_resumend_identif' => 'RR-202321-000001' ];
        $valores =   implode("','", array_values( $data ) )  ;
        $values =  implode(",", array_keys( $data ) )  ;

        $query = "INSERT INTO sh_cloud_cpe.tb_comunicacionbaja(".  $values. " ) VALUES( '" .  $valores . "' ) RETURNING tb_comunicacionb_cod";
        $result = $this->InsertOrUpdate( $query );
        return $result;
    }
    public function saveDataDet( array $data )
    {
        $valores =   implode("','", array_values( $data ) )  ;
        $values =  implode(",", array_keys( $data ) )  ;

        $query = "INSERT INTO sh_cloud_cpe.tb_comunicacionbaja_detalle(".  $values. " ) VALUES( '" .  $valores . "' )";
        $result = $this->InsertOrUpdate( $query );
        return $result;
    }


}
 ?>