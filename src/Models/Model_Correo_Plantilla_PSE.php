<?php 
// namespace Modelos;
// require '../Config/autoload.php';

namespace Models;
use Models\Model_Ose_Emisor;
// use CI_Model;
/**
 * Modelo para comprobantes
 */
class Model_Correo_Plantilla_PSE extends CI_Model
{
	// const TABLE_NAME = 'sh_cloud_pse.vw_codigobarra_comprobante';
	const TABLE_NAME = 'sh_cloud_pse.tb_correo_plantilla';
  const DEFAULT_TIPO_CORREO = 7;
  const field = [ 'id','cuerpo', 'asunto', 'con_copia' ];
  const BD_SERVER = 'postgres';
	function __construct()
	{
		parent::__construct(self::TABLE_NAME, 'postgres');	
	}
	

  public function getCorreoPlantillaByTypeAndRuc( $tipoCorreo = self::DEFAULT_TIPO_CORREO ,$codigoElmento = 0 ,  $ruc = null )
  {
    $query = "SELECT
              * 
            FROM
              sh_cloud_pse.tb_correo_plantilla,
              sh_cloud_pse.tb_tipo_correo 
            WHERE
              sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = sh_cloud_pse.tb_tipo_correo.id_tipo_correo 
              AND sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = '" . $tipoCorreo . "' 
              AND cod_elemento = '" . $codigoElmento . "' 
              ";
   
    $query .= $ruc ? " AND emisor = '" . $ruc . "'" : '';
    $result = $this->queryPersonalizate( $query );
    if ( !count( $result ) ) 
    {
       $query = "SELECT
              * 
            FROM
              sh_cloud_pse.tb_correo_plantilla,
              sh_cloud_pse.tb_tipo_correo 
            WHERE
              sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = sh_cloud_pse.tb_tipo_correo.id_tipo_correo 
              AND sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = '" . $tipoCorreo . "' 
              AND cod_elemento = '" . $codigoElmento . "' AND emisor = '00000000000' ";
   
      $result = $this->queryPersonalizate( $query );

      if ( !count( $result ) )
      {
        $query = "SELECT
              * 
            FROM
              sh_cloud_pse.tb_correo_plantilla,
              sh_cloud_pse.tb_tipo_correo 
            WHERE
              sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = sh_cloud_pse.tb_tipo_correo.id_tipo_correo 
              AND sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = '" .  self::DEFAULT_TIPO_CORREO . "' 
              AND cod_elemento = '" . $codigoElmento . "' AND emisor = '00000000000' ";
   
         $result = $this->queryPersonalizate( $query );
      }
    }
    // if ( $codigoElmento == 2 ) 
    // {
      return count( $result ) ? [ 'message' => $result[0][ 'valor_elemento' ] ] : [];  
    // }else
    // {
    //   return count( $result ) ? [ 'subjet' => $result[0][ 'valor_elemento' ] ] : [];  
    // }
    
  }

  /**
  * obtiene la configuracion de la plantilla
  */
  public function getPlantillaByRuc( $tipoCorreo = self::DEFAULT_TIPO_CORREO ,  $ruc = null )
  {
      $query = "SELECT
                * 
              FROM
                sh_cloud_pse.tb_correo_plantilla,
                sh_cloud_pse.tb_tipo_correo 
              WHERE
                sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = sh_cloud_pse.tb_tipo_correo.id_tipo_correo 
                AND sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = '" . $tipoCorreo . "'  ";
     
      $query .= $ruc ? " AND emisor = '" . $ruc . "'" : '';
      $result = $this->queryPersonalizate( $query );
      if ( !count( $result ) ) 
      {
         $query = "SELECT
                * 
              FROM
                sh_cloud_pse.tb_correo_plantilla,
                sh_cloud_pse.tb_tipo_correo 
              WHERE
                sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = sh_cloud_pse.tb_tipo_correo.id_tipo_correo 
                AND sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = '" . $tipoCorreo . "' 
                 AND emisor = '00000000000' ";
     
        $result = $this->queryPersonalizate( $query );

        if ( !count( $result ) )
        {
          $query = "SELECT
                * 
              FROM
                sh_cloud_pse.tb_correo_plantilla,
                sh_cloud_pse.tb_tipo_correo 
              WHERE
                sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = sh_cloud_pse.tb_tipo_correo.id_tipo_correo 
                AND sh_cloud_pse.tb_correo_plantilla.id_tipo_correo = '" .  self::DEFAULT_TIPO_CORREO . "' 
                 AND emisor = '00000000000' ";
     
           $result = $this->queryPersonalizate( $query );
        }
      }
     
      return count( $result ) ? $result[0] : [];  
  }


   public function get_emisor_impresion_pdf($ruc = '', $serie = '')
  {
    $query = "SELECT * from sh_cloud_pse.tb_emisor_impresion_pdf
              WHERE 
              tb_emisor_impresion_pdf_ruc = '" .$ruc. "'
              and tb_emisor_impresion_pdf_serie = '" . $serie . "'
              ";
    $result = $this->queryPersonalizate( $query );
    return  count( $result )>0 ? $result[0] : [];       
  }
  function get_email_template( Array $record )
  {

    $config = get_config();

    $modelOseEmisor = new Model_Ose_Emisor();

    $emisor = $modelOseEmisor->getEmisorByRuc( $record['ruc'] );
    
        if (  !count( $emisor ) )
        {
            return [];
        }
        $emisor = $emisor[0];
    $logo = $emisor['logo'] ? $config['url_frontend'] .'/'. $emisor['logo'] : '';
    // $logo = $modelOseEmisor->getEmisorByRuc( $porciones[0] );
        $logo = $logo ? "<img src='$logo' alt='Logo tipo' style='max-width: 100px; height: auto;'>" : '';

    $plantilla = $this->getPlantillaByRuc( $record[ 'tipo_correo' ] ,  $record[ 'ruc' ] );//body
    $result_impresion_pdf = $this->get_emisor_impresion_pdf( $record[ 'ruc' ] ,  $record[ 'serie' ] );

    $con_copia_template = count( $plantilla ) > 0 ? strtolower( str_replace( " " , "", $plantilla[ 'con_copia' ] ) )  : [];
    $con_copia_serie = '';
    if ( isset( $result_impresion_pdf[ 'tb_emisor_impresion_pdf_correo' ] ) ) 
    {
      $con_copia_serie = count( $result_impresion_pdf ) > 0 ?  strtolower( str_replace( " ", "", $result_impresion_pdf[ 'tb_emisor_impresion_pdf_correo' ] ) )   : [];  
    }
    $con_copia_serie_anulacion = '';
    if ( isset( $result_impresion_pdf[ 'tb_emisor_impresion_pdf_correo_anulacion' ] ) ) {
      $con_copia_serie_anulacion = count( $result_impresion_pdf ) > 0 ?  strtolower( str_replace( " ", "", $result_impresion_pdf[ 'tb_emisor_impresion_pdf_correo_anulacion' ] ) )   : [];
    }
   
    // var_dump($plantilla);
    // exit;
        // $subject = $this->getCorreoPlantillaByTypeAndRuc( $record[ 'tipo_correo' ] , $record[ 'ruc' ]);
        $nombreDocumento = get_type_doc_by_type( $record['tipo_cpe']  );

        if (!count($plantilla)) 
        {

            return ['message' => '', 'subject' => ''];
        }
        $subject = $plantilla[ 'asunto' ];
        $template['message'] = $plantilla[ 'cuerpo' ];
        $template['subject'] = $plantilla[ 'asunto' ];
        $template['con_copia'] = $con_copia_template;
        $template['con_copia_serie'] = $con_copia_serie;
        $template['con_copia_serie_anulacion'] = $con_copia_serie_anulacion;
        $alertaError =  isset( $record['alertaError'] ) ? $record['alertaError'] : '';
        $nombreProveedor =  isset( $record['nombre_proveedor'] ) ? $record['nombre_proveedor'] : '';

        $numeroDocumento = "{$record['serie']}-{$record['numero']}";
        $template['message'] = str_replace('{{fecha}}', $record['fecha'], $template['message']);
        $template['message'] = str_replace('{{numeroDocumento}}', $numeroDocumento, $template['message']);
        $template['message'] = str_replace('{{logotipo}}', $logo, $template['message']);
        $template['message'] = str_replace('{{nombreEmpresa}}', $emisor['descripcion'] ?: '-', $template['message']);
        $template['message'] = str_replace('{{nombreCliente}}', $record['razon_social'] ?: '-', $template['message']);
        $template['message'] = str_replace('{{nombreDocumento}}', $nombreDocumento, $template['message']);
        $template['message'] = str_replace('{{alertaError}}', $alertaError, $template['message']);
        $template['message'] = str_replace('{{nombreProveedor}}', $nombreProveedor, $template['message']);

        $styleColor_a = '';
        if (  $record['ruc'] == '20202380621' ||  $record['ruc'] == '20517182673' || $record['ruc']  == '20418896915' ) 
        {
          $styleColor_a = "style='color:black'";
        }
        $template['message'] = str_replace(
          '{{token_registro}}',  
          isset( $record['token_registro'] ) ? $record['token_registro'] : '', 
          $template['message']
        );
        $template['message'] = str_replace(
          '{{enlace_front_sesion_adquiriente}}',  
          (isset( $record['enlace_front_sesion_adquiriente'] ) && $record['enlace_front_sesion_adquiriente'] )? 
          "<a ".$styleColor_a." href='".$config['url_frontend']."/inicio/ingresar'>Click aqui para ingresar al portal</a>" : 
          '', 
          $template['message']
        );
        $template['message'] = str_replace(
          '{{enlace_front_registro_adquiriente}}',  
          ( isset( $record['enlace_front_registro_adquiriente'] ) && $record['enlace_front_registro_adquiriente'] )? 
          "<a ".$styleColor_a." href='".$config['url_frontend']."/inicio/registro_adquiriente'>Click aqui para registrarse en el Portal</a>" : 
          '', 
          $template['message']
        );
      // $template['message'] = str_replace('{{token_registro}}', $ruc_adquiriente_encrypt ?: '', $template['message']);

        if ( isset( $record['subject'] ) ) 
        {
           $numeroDocumento = "{$record['subject']['serie']}-{$record['subject']['numero']}";
        }
        $template['subject'] = str_replace('{{nombreDocumento}}', $nombreDocumento, $template['subject']);
        $template['subject'] = str_replace('{{numeroDocumento}}', $numeroDocumento, $template['subject']);
        $template['subject'] = str_replace('{{nombreEmpresa}}', $emisor['descripcion'], $template['subject']);
        return $template;
  }
}