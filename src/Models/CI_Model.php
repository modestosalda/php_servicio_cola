<?php  
namespace Models;
// require_once '../Config/autoload.php';
// include_once   $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/Modelos/Conexion.php';
// include_once   $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/helpers/common_helper.php';
// namespace Modelos;

// use Conexion;
/**
 * 
 */
class CI_Model extends Conexion
{

	private $table ;	
	public $server_bd ;
	private $modelCon ;
	function __construct( $table_name, $server_bd = 'postgres' )
	{

		$this->table = $table_name ;
		$this->server_bd =  $server_bd;

		$this->modelCon = parent::getInstance(   $server_bd );
		// echo "\n".$this->server_bd ."  : serverbd \n";

	}
	
	public function All()
	{

		try 
		{
			
			$this->modelCon->conexOpen( $this->server_bd );
			$query = 'select * from '. $this->table;
			$result = $this->modelCon->execQuery( $query );
			
			return $result;
			// $this->conexClose();


		} catch (Exception $e) {
			echo $e->getMessage();
			exit;
		}

	}
	// public function update(  )
	// {
	// 	$query = 'update ' . $this->table .
	// 	' set '
	// }
	public function queryPersonalizate( $query )
	{
		try 
		{	
		
			$this->modelCon->conexOpen( $this->server_bd );

			$table =  $this->table;
		
			$query = $query ;
			$result = $this->modelCon->execQuery( $query );
			return $result;
			// $this->conexClose();


		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	/**
	* Recibe un script por parametro para actualizar o insertar en una tabla
	*/
	public function InsertOrUpdate( $query )
	{
		
			try {
				$this->modelCon->conexOpen( $this->server_bd );
				// var_dump($this->modelCon);exit;
				$result = $this->modelCon->insert_update_pg( $query );
		        $message = "\n INSERT OR UPDATE DB REGISTRO: Se inserto o actualizo correctamente en la Tabla " . $this->table . ", QUERY:" . $query . " \n";
				
		        write_log( $message,1 );
		        // echo "<br>".$message."<br>";
			} catch (Exception $e) 
			{
				$error = "Error Al Guardar  o Actualizar la BD: '" . $this->table . "' Con el Query:". $query . " ARCHIVO: CI_MODEL.php Function InsertOrUpdate() \n" ;
				write_log("ERROR DB QUERY: " . $error  .", MENSAJE DB" . $e->getMessage() );
				echo json_encode([
					"error" =>  $error,
					"message_bd" =>  $e->getMessage(),
				],JSON_PRETTY_PRINT);
				throw new Exception($error , 1);
			}
			
			return $result;
			// $this->conexClose();

	}
	/**
	* Actualiza una tabla de la base de datos
	* @param $data la data a actualizar
	* @param  $where la condicion para actualizar
	* @param $talbe la tabla a actualizar
	*/
	public function CI_UPDATE( Array $data, Array $where ,  $tablePersonalizate = null)
	{
		$this->modelCon->conexOpen( $this->server_bd );
		$table = $tablePersonalizate!= null ? $tablePersonalizate: $this->table;
		try 
		{
			
			$dataKey = array_keys($data);
			$dataValue = array_values($data);

			$whereKey = array_keys($where);
			$whereValue = array_values($where);
			// var_dump($dataKey);
			// exit;

			$setQuery  = "UPDATE " . $table . " SET ";
			$i = 0;
			// echo count($whereValue) ;
			// exit;
			
			while ($i <= count($dataKey) - 1) 
			{
				
				$dataValue[ $i ]  = str_replace("'", '"', trim( $dataValue[ $i ] )) ;
				$dataValue[ $i ]  = ($dataValue[ $i ] == NULL ||  $dataValue[ $i ] == null) ? 'NULL' : "'" . $dataValue[ $i ]."'";
				$dataValue[ $i ]  = ($dataValue[ $i ] == "" &&  $dataValue[ $i ] != NULL )? "''" : $dataValue[ $i ];
				$setQuery.= $dataKey[ $i ] . " = " .  strval($dataValue[ $i ]) . ", ";
				
				// $setQuery.= $dataKey[ $i ] . " = NULL, ";
				
				$i++;
			}
			$setQuery = substr($setQuery, 0, -2);
			$setQuery .= " WHERE ";
			$i = 0;
			while ($i <= count($whereKey) -1 ) 
			{
				$whereValue[ $i ]  = trim( $whereValue[ $i ] );
				$whereValue[ $i ]  = $whereValue[ $i ] == NULL ? NULL : "'" . $whereValue[ $i ]."'";
				$whereValue[ $i ]  = $whereValue[ $i ] == "" ? "''" : $whereValue[ $i ];

				$setQuery.= $whereKey[ $i ] . " = " . $whereValue[ $i ]  ." and ";
				$i++;
			}
			$setQuery = substr($setQuery, 0, -4);
			
			$result = $this->modelCon->insert_update_pg( $setQuery );

	        $message = " UPDATE DB REGISTRO: Se actualizo correctamente en la Tabla " . $table . ", QUERY:" . $setQuery;

	        write_log( $message,1 );
	        return $result;
	        // echo "<br>".$message."<br>";
		}catch (Exception $e) 
		{
			$error = "Error Al Actualizar la BD: '" . $table . "' Con el Query:". $setQuery . " ARCHIVO: CI_MODEL.php Function InsertOrUpdate() , MENSAJE DB" . $e->getMessage() ;
			write_log("ERROR DB QUERY: " . $error  .", MENSAJE DB" . $e->getMessage() );
			throw new Exception($error, 1);
		}

	}
	/**
	* guarda la data desde un array
	* @param data array de datos a guardar
	* @param $returnning retorno del valor autoincrementable o el que se quiera que se retorne
	*/

	public function CI_SAVE(Array $data,Array $returning = []  ,$tablePersonalizate = null)
	{
		$this->modelCon->conexOpen( $this->server_bd );
		$table = $tablePersonalizate!= null ? $tablePersonalizate: $this->table;
		try 
		{
			$dataKey = array_keys($data);
			$dataValue = array_values($data);
			$setQuery  = "INSERT INTO " . $table . " ( ";
			$i = 0;
			
			$dataKeys = implode($dataKey, ',');
			$setQuery .= $dataKeys." ) VALUES( ";

			$i = 0;
			while ($i <= count($dataValue) -1 ) 
			{
				$dataValue[ $i ]  = str_replace("'", '"', trim( $dataValue[ $i ] )) ;
				$dataValue[ $i ]  = $dataValue[ $i ] == NULL ? NULL : "'" . $dataValue[ $i ]."'";
				$dataValue[ $i ]  = $dataValue[ $i ] == "" ? "''" : $dataValue[ $i ];

				$setQuery.= $dataValue[ $i ]  ." , ";
				$i++;
			}
			$setQuery = substr($setQuery, 0, -2) . ")";

			$setQuery .= count( $returning ) ? " RETURNING " .implode($returning, ',') : '';

			$result = $this->modelCon->insert_update_pg( $setQuery );

	        $message = " INSERT DB REGISTRO: Se inserto correctamente en la Tabla " . $table . ", QUERY:" . $setQuery;
	        write_log( $message,1 );
	        return $result;
	        // echo "<br>".$message."<br>";
		}catch (Exception $e) 
		{
			$error = "Error Al INSETAR la BD: '" . $table . "' Con el Query:". $setQuery . " ARCHIVO: CI_MODEL.php Function CI_SAVE() , MENSAJE DB" . $e->getMessage() ;
			write_log("ERROR DB QUERY: " . $error  .", MENSAJE DB" . $e->getMessage() );
			echo json_encode([
				"error" =>  $error,
				"message_bd" =>  $e->getMessage(),
			],JSON_PRETTY_PRINT);
			// return $error;
			throw new Exception($error , 1);
		}
	}

}

?>