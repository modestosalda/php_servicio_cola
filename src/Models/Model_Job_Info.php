<?php 
namespace Models;
// use CI_Model;
/**
 * Modelo para comprobantes
 */
class Model_Job_Info extends CI_Model
{
	// const TABLE_NAME = 'sh_cloud_pse.vw_codigobarra_comprobante';
	const TABLE_NAME = 'job_info';
	function __construct()
	{
		parent::__construct(self::TABLE_NAME);	
	}
    /**
    *   
    */
    function getJobActiveByType( $type = '' ):Array
    {
        $query = "SELECT * from ". self::TABLE_NAME . " where status is null ";
        if ( $type != '' )
        {
            $query.= " and type = '" . $type. "' ";
        }
        $query .= 'limit 1';
        $result = $this->queryPersonalizate( $query );
        if ( count($result) > 0) 
        {
            $id = $result[ 0 ][ 'id' ];
            $data = [
                'status' => 'P'
            ];
            $where = [
                'id' => $id
            ];
            // $this->CI_UPDATE( $data, $where );
        }
        return  count( $result ) > 0 ? $result[0] : [];
    }
    function deleteJob( $id )
    {
        $query = "delete from ". self::TABLE_NAME . " where id= '" .$id. "'";
        $this->queryPersonalizate($query);
    }
    public function getJobCpe( $nroCola = false )
    {
        // $query = 'select * from get_cola_update_job('.intval( $nroCola ).');';
        $query = "SELECT job_info.*,
        tb_emisor_ws::text, 
        url_servicio_retenciones::text ,
        url_servicio_guias::text,
        tb_emisor_user_sol::text, 
        tb_emisor_clave_sol::text, 
        tb_emisor_tipo_operador::text 
        from job_info,sh_cloud_pse.tb_emisor where tb_emisor.tb_emisor_ruc=job_info.ruc_emisor and estado is null  and tipo_job='E' ";
        if ( $nroCola !== false ) {

            $query = " and nro_cola='".$nroCola."'";
        }
        $result = $this->queryPersonalizate($query);
        return count($result) > 0 ? $result : [];
    }
    public function updateJob( $data )
    {
        $tb_comprobante_id = array_map( function( $element ){ return $element['tb_comprobante_id'] ;} , $data );
        $tb_comprobante_id = implode(',', $tb_comprobante_id);
       
        $query = "UPDATE job_info 
        SET estado = 'P'
        where tb_comprobante_id in ($tb_comprobante_id)
        ";
        $result = $this->InsertOrUpdate($query);
    }


}
 ?>