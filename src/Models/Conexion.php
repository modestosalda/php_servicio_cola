<?php 
// error_reporting(E_ALL);
namespace Models;

// namespace Modelos;
/**
 * class de conexion a BASE DE Datos
 */
class Conexion
{
	// private $_host;
	public $_server_database;
	// private static $_server_database;
	private static $instance = NULL;
	// private $_name_database;
	// private $_user_name;
	// private $_password;
	public $cn  = false;
	public $cnMysql = false;

	/**
	* Constructor
	* @param $host ubicacion de la base de datos
	* @param $server_database servidor de base de datos, EJ: mysql, postgress, oracle, etc.
	* @param $name_database nombre de la base de tados.
	* @param $user_name nombre de usuario.
	* @param $password contraseña de la base de datos.
	*/
	// function __construct( $server_database =  'postgres')
	function __construct( $server_database =  'postgres')
	{

		$this->_server_database = $server_database;
		echo "hemos creado un nuevo objeto<br>";

		
	}
	public static function getInstance(  $server_database )
    {
      	if (is_null(self::$instance)) {
        	// self::$_server_database = $server_database;
            echo "\nObjeto CONEXION CREADO \n";
           self::$instance = new Conexion( $server_database );
        } else {
            // echo "El objeto CONEXION ya existe, no puedes volver a crearlo \n";
        }
        return self::$instance;
    }
	public function serverName($serverName )
	{
		$this->_serverName = $serverName;
	}

	/**
	* Inicia una conexion a la base de dtaos
	*/
	public function conexOpen( $server_database )
	{
		$this->_server_database = $server_database;

		require __DIR__ .'/../config/database.php';
			
			// $con_database = $db['conex'][ self::$_server_database ];
			$con_database = $db['conex'][ $this->_server_database ];
			switch ( $this->_server_database ) 
			{
				case 'postgres' :
					if ( !$this->cn  ) 
					{
						
						$cnCad = "host=" . $con_database[ 'hostname' ] . " port=5432 dbname=".  $con_database[ 'database' ] ." user=" . $con_database[ 'username' ] . " password=" . $con_database[ 'password' ] . " options='--client_encoding=UTF8'";

						$this->cn = pg_connect( $cnCad );
						echo "\ninstaciado a conexion a postgres\n";
						if ( $this->cn ) 
				        {
				            return [ 'status' => true, 'message' => 'conexion exitosa' ];
				        } else 
				        {
				           throw new Exception("Error de conexion a la base de datos", 1);
				           
				        }
				    }
				    else{
						// echo "\nya esta instaciado a conexion postgres\n";
					}
					break;
				case 'mysql' :
					if ( !$this->cnMysql  ) 
					{
						$this->cnMysql =new \mysqli( $con_database[ 'hostname' ], $con_database[ 'username' ] , $con_database[ 'password' ], $con_database[ 'database' ]  ,3306);
						echo "\ninstaciado a conexion a mysql\n";
						 if($this->cnMysql->connect_errno)
						 {
			                throw new Exception("Error al conectarse a la BDMYSQL", 1);
			            }else{
			                return [ 'status' => true, 'message' => 'conexion exitosa' ];
			                return $this->estado;
			            }
			        }else{
						// echo "\nya esta instaciado a conexion mysql\n";
			        }
					break;
			};
		
		
		
	}
	// public function conexClose()
	// {
	// 	switch ( $this->_server_database ) 
	// 	{
	// 		case 'postgres':
	// 			pg_close( $this->cn );
	// 			break;
			
	// 	}
	// 	return true;
	// }
	public function execQuery( $query ) 
	{
		
        $result = array();
		switch ( $this->_server_database ) 
		{
			case 'postgres':

					$res = pg_query($this->cn,$query) or die( "Respuesta: " .pg_last_error($this->cn). '  Query :'. $query  );
					while ($row = pg_fetch_assoc($res)) 
					{
				            $result[] = $row;
				    }
				    break;
			case 'mysql':
			       	$resultado = $this->cnMysql->query($query) or die( mysql_error($this->cnMysql) );
		            while($row = $resultado->fetch_assoc()) 
		            {
		                $result[] = $row;
		            };
				break;
			
			default:
				# code...
				break;
		}
		
        
        return $result;

	}
	public function insert_update_pg( $query )
	{
		try {
			$res = pg_query($this->cn ,$query) ;
			if ( $res ) 
			{
				$resultados = pg_fetch_array($res);
				$last_val = 0;
				if ( isset( $resultados[0] ) ) {
					$last_val = $resultados[0];
				}

				return [ 'status' => true , 'last_val' => $last_val ];
			}
			throw new \Exception( 'Respuesta: '. pg_last_error( $this->cn ). ' Query: '. $query, 1);	
		} catch (Exception $e) {
			throw new \Exception( 'Respuesta: '. pg_last_error( $this->cn ). ' Query: '. $query, 1);	
		}
		
		
			
			 
		// } catch (Exception $e) 
		// {
		// 	return [ 'error' => $e->getMessage() , 'status' => false];	
		// }
		
	}
	function __destruct()
	{
		if ( $this->cn ) 
		{
			pg_close( $this->cn );
			echo "\n LA SESION PGSQL SE CIERRA \n";
		}
		if ( $this->cnMysql ) 
		{
			mysqli_close ( $this->cnMysql );
			echo "\n LA SESION MYSQL SE CIERRA \n";
		}
	}

}

 ?>