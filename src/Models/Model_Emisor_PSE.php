<?php 
// namespace Modelos;
// require '../Config/autoload.php';
Include_once   $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/Modelos/CI_Model.php';
namespace Models;

// use CI_Model;
/**
 * Modelo para Emisores PSE
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */

class Model_Emisor_PSE extends CI_Model
{
	const TABLE_NAME = 'sh_cloud_pse.tb_emisor';
    const BD_SERVER = 'postgres';

	function __construct()
	{
		parent::__construct(self::TABLE_NAME);	
	}
    /**
    * retorna el emisor por ruc
    */
    function getEmisorByRuc( String $ruc )
    {
        $script = "select * from " . self::TABLE_NAME . " where tb_emisor_ruc = '" .$ruc. "' " ;
        $result = $this->queryPersonalizate( $script );
        return count( $result ) ? $result[0] : [];
    }
    /**
    * setea el array del emisor para el envio al servicio de emision
    * retorna el array 
    */
    public function setOEmisor( $data, $codigoTipoDocumentoIdentidad = "6", $codigoEstablecimientoAnexo= "0000" )
    {
        $array = 
        [
            'apellidosNombresDenominacionRazonSocial' => $data[ 'tb_emisor_nombres' ],
            'codigoTipoDocumentoIdentidad' =>$codigoTipoDocumentoIdentidad,
            'codigoEstablecimientoAnexo' =>$codigoEstablecimientoAnexo,
            'urbanizacion' => $data[ 'tb_emisor_urbanizacion' ],
            'codigoUbicacionGeografica' => $data[ 'tb_emisor_ubigeo' ],
            'numeroDocumentoIdentidad' => $data[ 'tb_emisor_ruc' ],
            'nombreComercial' => $data[ 'tb_emisor_nombrecomercial' ],
            'distrito' => $data[ 'tb_emisor_distrito' ],
            'provincia' => $data[ 'tb_emisor_provincia' ],
            'departamento' => $data[ 'tb_emisor_departamento' ],
            'codigoPais' => $data[ 'tb_emisor_codigopais' ],
            'direccion' => $data[ 'tb_emisor_direccion' ]
        ];
        return $array;
    }

    


}
 ?>