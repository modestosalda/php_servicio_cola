<?php 
// namespace Modelos;
// require '../Config/autoload.php';
Include_once   $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/Modelos/CI_Model.php';
namespace Models;

// use CI_Model;
/**
 * Modelo para comprobantes
 * Created By Modesto Salda�a Michalec<modesto_salda_m93@gmail.com>
 */
class Model_Percepcion_CPE extends CI_Model
{
	const TABLE_NAME = 'sh_cloud_cpe.tb_percepcion_cab';
	function __construct()
	{
		parent::__construct(self::TABLE_NAME);	
	}
    /**
    * Actualiza los campos "tipo_doc_rela, estado_doc_rela , num_doc_rela"
    */
    public function actualizarPercepcionByFileAndType($tipo_doc_rela, $estado, $numIdentidad, $file )
    {
        $arrayFile = explode('-', $file);
        $correlativo = str_replace('.zip', '', $arrayFile[ 3 ]);
        $pg_script = "update " . self::TABLE_NAME . "
                    set tipo_doc_rela='" . $tipo_doc_rela . "', estado_doc_rela = '".  $estado ."', num_identidad_doc_rela = '" .  $numIdentidad . "'
                    where cpe_num_ruc = '" .  $arrayFile[ 0 ] . "' and cpe_num_serie  = '" .  $arrayFile[ 2 ] . "' and cpe_num_correl  = '" . $correlativo  . "'";
        $result = $this->InsertOrUpdate( $pg_script );
        return $result;
        
    }

    
        /**
    * Actualiza el estado"
    */
    public function actualizarEstadoPercepcionByFileAndType( $tipo_doc_rela, $estado, $numIdentidad, $file )
    {
        $arrayFile = explode('-', $file);
        $correlativo = str_replace('.zip', '', $arrayFile[ 3 ]);
        $pg_script = "update " . self::TABLE_NAME . "
                    set tipo_doc_rela='" . $tipo_doc_rela . "', estado_doc_rela = '".  $estado ."', num_identidad_doc_rela = '" .  $numIdentidad . "'
                    where cpe_num_ruc = '" .  $arrayFile[ 0 ] . "' and cpe_num_serie  = '" .  $arrayFile[ 2 ] . "' and cpe_num_correl  = '" . $correlativo  . "'";
        $result = $this->InsertOrUpdate( $pg_script );
        return $result;
        
    }

    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveCabXmlPER(Array $dataXml )
    {
        $datosEmisor = $dataXml[ 'datos_emisor' ];
        $detalleCpe = $dataXml[ 'detalle_cpe' ];
        $datosAdquiriente = $dataXml[ 'datos_adquiriente' ];
        $datosGlobales = $dataXml[ 'datos_globales' ];
        $dataSave=[
            'cpe_num_ruc' => $datosEmisor['ruc_emisor'],
            
            'cpe_num_serie' => $detalleCpe['numeroSerie'],
            'cpe_num_correl' => $detalleCpe['numeroCorrelativo'],
            'cpe_fecha' => $detalleCpe['fecha'] . ' ' . $detalleCpe['hora'],
            'cpe_cod_mone' => $detalleCpe['codigoTipoMoneda'],
            'cpe_reg' => $detalleCpe['cpe_reg'],

            'clie_tip_doc' => $datosAdquiriente['tipodocumentocliente'],
            'clie_num_doc' => $datosAdquiriente['numeroDocumentoIdentidad'],
            'clie_ubigeo' => $datosAdquiriente['clie_ubigeo'],
            'clie_rz_social' => $datosAdquiriente['clie_razonsocial'],
            'clie_distrito' => $datosAdquiriente['distrito'],
            'clie_provincia' => $datosAdquiriente['provincia'],
            'clie_codpais' => $datosAdquiriente['cod_pais'],
            'clie_dpto' => $datosAdquiriente['departamento'],
            'clie_urb' => $datosAdquiriente['urbanizacion'],
            'clie_dir' => $datosAdquiriente['dir_completa'],

            'mnt_per' => $datosGlobales['mnt_per'],
            'mnt_cob' => $datosGlobales['mnt_cob_per'],
            'cpe_cod_mone' => $datosGlobales['cpe_cod_mone'],
        ];
        $returning = [
            'num_id'
        ];
        $result = $this->CI_SAVE( $dataSave,  $returning );
        
        return $result;
    }

    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveDetXmlPER(Array $dataXml )
    {

        foreach ($dataXml as  $itemData) 
        {
            $dataSave = $itemData;
            $result = $this->CI_SAVE( $dataSave ,[],'sh_cloud_cpe.tb_percepcion_det');
        }

        return $result;
    }

    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveNotaXmlPER(Array $dataXml )
    {

        foreach ($dataXml as  $itemData) 
        {
            $dataSave = $itemData;
            $result = $this->CI_SAVE( $dataSave ,[],'sh_cloud_cpe.tb_percepcion_notas');
        }

        return $result;
    }
    public function saveDataCab( array $data )
    {
        $valores =   implode("','", array_values( $data ) )  ;
        $values =  implode(",", array_keys( $data ) )  ;

        $query = "INSERT INTO " . self::TABLE_NAME ."(".  $values. " ) VALUES( '" .  $valores . "' ) RETURNING  num_id";
        $result = $this->InsertOrUpdate( $query );
        return $result;
    }
    public function saveDataDet( array $data )
    {
        $valores =   implode("','", array_values( $data ) )  ;
        $values =  implode(",", array_keys( $data ) )  ;

        $query = "INSERT INTO sh_cloud_cpe.tb_percepcion_det(".  $values. " ) VALUES( '" .  $valores . "' ) RETURNING  id";
        $result = $this->InsertOrUpdate( $query );
        return $result;
    }
    public function saveDataNotas( array $data )
    {
        $valores =   implode("','", array_values( $data ) )  ;
        $values =  implode(",", array_keys( $data ) )  ;

        $query = "INSERT INTO sh_cloud_cpe.tb_percepcion_notas(".  $values. " ) VALUES( '" .  $valores . "' ) ";
        $result = $this->InsertOrUpdate( $query );
        return $result;
    }

}
 ?>