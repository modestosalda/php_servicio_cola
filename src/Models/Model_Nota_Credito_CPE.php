<?php 
namespace Models;
/**
 * Modelo para comprobantes
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */
class Model_Nota_Credito_CPE extends CI_Model
{
	const TABLE_NAME = 'sh_cloud_cpe.tb_nc_cab';
	function __construct()
	{
		parent::__construct(self::TABLE_NAME);	
	}
    /**
    * Actualiza los campos "tipo_doc_rela, estado_doc_rela , num_doc_rela"
    */
    public function actualizarNCByFileAndType( $tipo_doc_rela, $estado, $numIdentidad, $file )
    {
        $arrayFile = explode('-', $file);
        $correlativo = str_replace('.zip', '', $arrayFile[ 3 ]);
        $pg_script = "update " . self::TABLE_NAME . "
                    set tipo_doc_rela='" . $tipo_doc_rela . "', estado_doc_rela = '".  $estado ."', num_identidad_doc_rela = '" .  $numIdentidad . "'
                    where cpe_num_ruc = '" .  $arrayFile[ 0 ] . "' and cpe_num_serie  = '" .  $arrayFile[ 2 ] . "' and cpe_num_correl  = '" . $correlativo  . "'";
        $result = $this->InsertOrUpdate( $pg_script );
        return $result;
        
    }
    /**
    * retorna el maximo numero correlativo y la suma del ultimo +1
    * @param $ruc ruc del emisor
    * @param $serie serie del comprobante
    */
    public function getMaxCorrelativoNC( String $ruc,String  $serie )
    {
        $script = "select MAX(CAST(cpe_num_correl AS int))::text correlativo_max , (MAX(CAST(cpe_num_correl AS int)) +1 ) as correl_next 
            from " . self::TABLE_NAME . "
            where tb_nc_cab.cpe_num_ruc = '". $ruc . "' 
            and tb_nc_cab.cpe_num_serie = '". $serie . "' ";
        $result = $this->queryPersonalizate( $script );
        return count( $result ) ? $result[0] : [];
    }

    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveCabXmlNC(Array $dataXml, $dataDodAfecta )
    {
        $datosEmisor = $dataXml[ 'datos_emisor' ];
        $detalleCpe = $dataXml[ 'detalle_cpe' ];
        $datosAdquiriente = $dataXml[ 'datos_adquiriente' ];
        $datosGlobales = $dataXml[ 'datos_globales' ];
        $datosPercepcion = $dataXml[ 'percepcion' ];
        $datosOrdenCompra = $dataXml[ 'ordenes_compra_guia' ];

        $dataSave=[
            'cpe_num_ruc' => $datosEmisor['ruc_emisor'],
            'cpe_num_serie' => $detalleCpe['numeroSerie'],
            'cpe_num_correl' => $detalleCpe['numeroCorrelativo'],
            'cpe_fecha' => $detalleCpe['fecha'] . ' ' . $detalleCpe['hora'],
            'cpe_cod_opera' => $detalleCpe['codigoTipoOperacion'],
            'cpe_cod_mone' => $detalleCpe['codigoTipoMoneda'],

            'afect_serie_corre'=> $dataDodAfecta[ 'afect_serie_corre' ],
            'tipo_nc'=> $dataDodAfecta[ 'tipo_nc' ],
            'motivo_nc'=> $dataDodAfecta[ 'motivo_nc' ],
            'modif_serie_corre'=> $dataDodAfecta[ 'modif_serie_corre' ],
            'modif_tipo_doc'=> $dataDodAfecta[ 'modif_tipo_doc' ],

            'clie_tip_doc' => $datosAdquiriente['tipodocumentocliente'],
            'clie_num_doc' => $datosAdquiriente['numeroDocumentoIdentidad'],
            'clie_ubigeo' => $datosAdquiriente['clie_ubigeo'],
            'clie_rz_social' => $datosAdquiriente['clie_razonsocial'],
            'clie_distrito' => $datosAdquiriente['distrito'],
            'clie_provincia' => $datosAdquiriente['provincia'],
            'clie_codpais' => $datosAdquiriente['cod_pais'],
            'clie_dpto' => $datosAdquiriente['departamento'],
            'clie_urb' => $datosAdquiriente['urbanizacion'],
            'clie_dir' => $datosAdquiriente['dir_completa'],

            'mnt_imp' => $datosGlobales['montoImpuestoMonedaOriginal'],
            'mnt_importe' => $datosGlobales['montoValorVentaMonedaOriginal'],
            'mnt_total' => $datosGlobales['montoTotalMonedaOriginal'],

            'cargodes_cod_reg_per' => $datosPercepcion['codigoRegimenPercepcion'],
            'cargodes_porcentaje' => $datosPercepcion['porcentajePercepcion'],
            'cargodes_mnt_perc' => $datosPercepcion['montoPercepcionMonedaNacional'],
            'cargodes_mntbase_perc' => $datosPercepcion['montoBasePercepcionMonedaNacional'],
            'cargodes_importe_con_perc' => $datosPercepcion['importeTotalConPercepcionMonedaNacional'],

            'orden_compra' => $datosOrdenCompra['ordenes_compra'],
            'tipo_doc_guia' => $datosOrdenCompra['tipo_doc_guiaremi'],
            'guia_remi' => $datosOrdenCompra['guia_remi'],

        ];
        $returning = [
            'num_id'
        ];
        $result = $this->CI_SAVE( $dataSave,  $returning );
        
        return $result;
    }
    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveDetXml(Array $dataXml )
    {
        foreach ($dataXml as  $itemData) 
        {
            $dataSave = [
                'num_id' => $itemData[ 'num_id' ],
                'item_des' => $itemData[ 'item_des' ],
                'item_cod' => $itemData[ 'item_cod' ],
                'item_cod_sunat' => $itemData[ 'item_cod_sunat' ],
                'item_und' => $itemData[ 'codigoUnidadMedida' ],
                'item_cant' => $itemData[ 'cantidad' ],
                'item_pu' => $itemData[ 'PriceAmountXX' ],
                'item_mnt_unt' => $itemData[ 'montoValorVentaUnitarioMonedaOriginal' ],
                'item_mnt_valor_total' => $itemData[ 'montoValorVentaTotalMonedaOriginal' ],
                'imp_cod_afec_trib' => $itemData[ 'TaxExemptionReasonCode' ],
                'imp_mnt_total_trib' => $itemData[ 'montoTotalMonedaOriginalXX' ],
                'imp_mnt_base' => $itemData[ 'montoBaseMonedaOriginalXX' ],
                'imp_nombre' => $itemData[ 'nombreXX' ],
                'imp_cod_inter' => $itemData[ 'codigoInternacionalXX' ],
                'imp_cod_trib' => $itemData[ 'IDXX' ],
                'imp_porc' => $itemData[ 'PercentXX' ]
            ];
            $result = $this->CI_SAVE( $dataSave ,[],'sh_cloud_cpe.tb_factura_det');
        }

        return $result;
    }
    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveNotaXml(Array $dataXml )
    {
        foreach ($dataXml as  $itemData) 
        {

            $dataSave = [
                'num_id' => $itemData[ 'num_id' ],
                'codigo' => $itemData[ 'codigo_nota' ],
                'descripcion' => $itemData[ 'descripcion_nota' ]
            ];
            $result = $this->CI_SAVE( $dataSave,[],'sh_cloud_cpe.tb_factura_notas' );
        }

        return $result;
    }
    /**
    * guarda la informacion seteada desde un xml tranformada en array
    */
    public function saveTributoXml(Array $dataXml )
    {
        foreach ($dataXml as  $itemData) 
        {
            $dataSave = 
            [
                'num_id' => $itemData[ 'num_id' ],
                'imp_mnt_total_tri'=> $itemData[ 'montoTotaltributo' ],
                'imp_mnt_base'=> $itemData[ 'montoBaseMonedaOriginal' ] ,
                'imp_nombre'=>$itemData[ 'nombre' ] ,
                'imp_cod_inter'=> $itemData[ 'codigoInternacional' ],
                'imp_cod_trib'=> $itemData[ 'codigo_impuesto' ] 
            ];

            $result = $this->CI_SAVE( $dataSave,[],'sh_cloud_cpe.tb_factura_tri' );
        }

        return $result;
    }

}
 ?>