<?php 
// namespace Modelos;
// require '../Config/autoload.php';
Include_once   $_SERVER[ 'DOCUMENT_ROOT' ] . '/ws/Modelos/CI_Model.php';
namespace Models;

// use CI_Model;
/**
 * Modelo Resumen Diario
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */
class Model_FE_Recibidos_PSE extends CI_Model
{
	const TABLE_NAME = 'sh_cloud_pse.fe_recibidos';
	function __construct()
	{
		parent::__construct(self::TABLE_NAME);	
	}
    
    
   public function getDataByDateAndRuc($json )
   {
    $script = "SELECT codigo, to_char(fecha_ingreso, 'YYYY-MM-DD') fecha_ingreso,".
        "mes, dia, tipo_dte, folio, serie, correlativo, fecha_emision, mes_emision,".
        "dia_emision, fecha_vencimiento, mes_vencimiento, dia_vencimiento, id_emisor,".
        "id_receptor, tipo_documento_receptor, nombre_receptor, monto_neto, monto_total,".
        "monto_exento, fecha_ult_modificacion, estado, canal, digest, fecha_firma, uri,".
        "uri_pdf, uri_arm, estado_arm, fecha_arm, uri_nar, estado_nar, mensaje_nar, fecha_nar,".
        "uri_er, estado_er, fecha_er, dia_er, mensaje_er, hash_fe, estado_intercambio,".
        "estado_mandato, case when cod_moneda = 'PEN' THEN 'SOLES' WHEN cod_moneda = 'USD' then 'DOLAR' else cod_moneda end cod_moneda,".
        "pais, mensaje_arm, data_dte, referencias, impuestos, parametro1, parametro2, parametro3,".
        "parametro4, parametro5, observacion_contable, periodo_contable, nombre_emisor FROM sh_cloud_pse.fe_recibidos WHERE 1=1 ";
        /*if ( !empty( $dateIni) && !empty($dateFin) )
        {
            $script .= "AND fecha_ingreso::date >= '". $dateIni ."'::date
                    AND fecha_ingreso::date <= '". $dateFin ."'::date";
        }
        if ( !empty($ruc_receptor)  ) 
        {
            $script.=" AND id_receptor = '" . $ruc_receptor . "' ";
        }
        */
       $script.= " AND (estado in('" . $json[ "estados" ] . "') or estado is null)";
       if ( !empty($json[ "id_receptor" ] ) ) {
           $script.= " AND id_receptor='" . $json[ "id_receptor" ] . "'";
       }

       if ( !empty($json[ "ruc_emisor" ] ) ) {
           $script.= " AND id_emisor='" . $json[ "ruc_emisor" ] . "'";
       }
       if ( !empty($json[ "serie" ] ) ) {
           $script.= " AND serie='" . $json[ "serie" ] . "'";
       }
       if ( !empty($json[ "correlativo" ] ) ) {
           $script.= " AND correlativo='" . $json[ "correlativo" ] . "'";
       }
       if ( !empty($json[ "correlativo" ] ) ) {
           $script.= " AND correlativo='" . $json[ "correlativo" ] . "'";
       }
       if ( !empty($json[ "tipo_doc" ] ) ) {
           $script.= " AND tipo_dte='" . $json[ "tipo_doc" ] . "'";
       }
       if ( !empty( $json[ "fechainicio" ] ) && !empty($json[ "fechafin" ] ) )
       {
           $fechaInicio = str_replace ('-', '', $json[ "fechainicio" ]);
           $fechaFin = str_replace ('-', '', $json[ "fechafin" ]);

           if ( $json[ "tipo_fecha" ] == 1 ) {
               $script .= " AND dia between '" . $fechaInicio . "' AND '" . $fechaFin . "'";
           } else {
               $script .= " AND dia_emision between '" . $fechaInicio . "' AND '" . $fechaFin . "'";
           }
       }

         $script .= " limit 100";
        $result = $this->queryPersonalizate( $script );
        return $result;
   }
    public function saveData(array $dataXml  )
    {
          // $dataSave = [ 'tb_resumend_ruc' => 2121336546 , 'tb_resumend_identif' => 'RR-202321-000001' ];
        $detalleCpe = $dataXml[ 'detalle_cpe' ];
        $datosEmisor = $dataXml[ 'datos_emisor' ];
        $datosAdquiriente = $dataXml[ 'datos_adquiriente' ];
        $datosGlobales = $dataXml[ 'datos_globales' ];
        $dataRes = $dataXml[ 'data_res' ];//datps de resuesta
        $dataSave = [
            'fecha_ingreso' => date('Y-m-d H:i:s'),//mes actual
            'mes' => date('m'),//mes actual
            'dia' => date('d'),//dia actual
            'tipo_dte' => $detalleCpe[ 'codigoTipoDocumento' ],//tipo documento
            'folio' => $detalleCpe[ 'numeroSerie' ].'-'.$detalleCpe[ 'numeroCorrelativo' ], 
            'serie' => $detalleCpe[ 'numeroSerie' ],
            'correlativo' =>$detalleCpe[ 'numeroCorrelativo' ] ,
            'fecha_emision' => $detalleCpe[ 'fecha' ] ,
            'cod_moneda'=> $detalleCpe[ 'codigoTipoMoneda' ],
            'mes_emision' => date("m", strtotime($detalleCpe[ 'fecha' ] ) ),
            'dia_emision' => date("d", strtotime($detalleCpe[ 'fecha' ] ) ),
            'id_emisor' => $datosEmisor['ruc_emisor'],//ruc del emisor
            
            'id_receptor' =>$datosAdquiriente[ 'numeroDocumentoIdentidad' ] ,//ruc del receptor
            // 'fecha_vencimiento' => '',
            // 'mes_vencimiento' => '',
            // 'tipo_documento_receptor' => ,
            // 'nombre_receptor' => ,
            'monto_total' =>  $datosGlobales['montoTotalMonedaOriginal'],
            // 'monto_exento' => ,
            // 'fecha_ult_modificacion' => ,//valor por defecto
            // 'estado' => '',
            'canal' => 'FRONT',
            // 'digest' => '',
            'uri' => $dataXml['uri'],
            'uri_er' => $dataXml['uri_er'],
            'uri_pdf' => $dataXml['uri_pdf'],
        ];

        if ( count($dataRes) ) 
        {
            $dataSave[ 'estado_er' ] = $dataRes[ 'estado_er' ];
            $dataSave[ 'fecha_er' ] = $dataRes[ 'fecha_er' ]. ' '. $dataRes[ 'hora_er' ];
            $dataSave[ 'dia_er' ] =  date("d", strtotime($detalleCpe[ 'fecha_er' ] ) );
            $dataSave[ 'mensaje_er' ] = $dataRes[ 'mensaje' ];
        }

        $valores =   implode("','", array_values( $dataSave ) ) ;
        $values =  implode(",", array_keys( $dataSave ) )  ;

        $query = "INSERT INTO " . self::TABLE_NAME ."(".  $values. " ) VALUES( '" .  $valores . "' )";
        $result = $this->InsertOrUpdate( $query );
        return $dataSave;
    }
    public function saveDataDet( array $data )
    {
        $valores =   implode("','", array_values( $data ) )  ;
        $values =  implode(",", array_keys( $data ) )  ;

        $query = "INSERT INTO sh_cloud_cpe.tb_comunicacionbaja_detalle(".  $values. " ) VALUES( '" .  $valores . "' )";
        $result = $this->InsertOrUpdate( $query );
        return $result;
    }


}
 ?>
