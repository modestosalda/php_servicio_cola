<?php 
// namespace Modelos;
// require '../Config/autoload.php';
namespace Models;

// use CI_Model;
/**
 * Modelo para comprobantes
 * Created By Modesto Saldaña Michalec<modesto_salda_m93@gmail.com>
 */
class Model_Ose_Emisor extends CI_Model
{
    const TABLE_NAME = 'sh_cloud_cpe.tb_percepcion_cab';
	const BD_SERVER = 'mysql';
	function __construct()
	{
		parent::__construct(self::TABLE_NAME, self::BD_SERVER);	
        
	}
    public function getEmisorByRuc( $ruc )
    {
        $query  = "SELECT
                    * 
                    FROM
                    ose_emisor 
                    WHERE
                    ose_emisor.ruc ='" . $ruc. "'" ;
        $result = $this->queryPersonalizate( $query );
        return $result;
    }
    public function getUsuarioEmisorByRuc( $ruc )
    {
        $query = "SELECT username,password_2 from ose_aso_emisores_usuarios rel 
                    inner join ose_usuario_emisor ou on rel.usuario_id=ou.id 
                    inner join ose_emisor oe on rel.emisor_id=oe.id
                    where ruc='" . $ruc . "' ";
        $result = $this->queryPersonalizate( $query );
        return count($result) ? $result[0] : [];
    }
}
 ?>