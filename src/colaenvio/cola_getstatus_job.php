<?php
error_reporting(E_ALL);
include "conexPGSQL.php";
include 'sunat_ose.php';
include  $_SERVER['DOCUMENT_ROOT'] . '/ws/Modelos/Model_Factura_Cab_CPE.php';
include  $_SERVER['DOCUMENT_ROOT'] . '/ws/Modelos/Model_Nota_Credito_CPE.php';
include  $_SERVER['DOCUMENT_ROOT'] . '/ws/Modelos/Model_Nota_Debito_CPE.php';
include  $_SERVER['DOCUMENT_ROOT'] . '/ws/Modelos/Model_Percepcion_CPE.php';
include  $_SERVER['DOCUMENT_ROOT'] . '/ws/Modelos/Model_Retencion_CPE.php';

include  $_SERVER['DOCUMENT_ROOT'] . '/ws/Modelos/Model_Comprobantes_PSE.php';
include  $_SERVER['DOCUMENT_ROOT'] . '/ws/Modelos/Model_Documentos_Referenciados_PSE.php';
// require  $_SERVER['DOCUMENT_ROOT'] . "/ws/sendgrid-php/sendgrid-php.php";
require  $_SERVER['DOCUMENT_ROOT'] . "/ws/lib/APIv3-php-library/sendinblue.php";
include  $_SERVER[ 'DOCUMENT_ROOT' ]. "/ws/helpers/conex_sunat_ose.php";
include  $_SERVER[ 'DOCUMENT_ROOT' ]. "/ws/helpers/common_helper.php";
include $_SERVER[ 'DOCUMENT_ROOT' ]. "/ws/helpers/XML_Comprobante.php";
include $_SERVER[ 'DOCUMENT_ROOT' ]. "/ws/helpers/XML_ComprobantePerRet.php";

// $numCola=  isset( $_GET['cola']) ? $_GET['cola'] : false;
$directorio_cola =  $_SERVER[ 'DOCUMENT_ROOT' ]. "/ws/colaenviocpe/";

$file_cola =$directorio_cola. "cola.json";


$gestionRequestSunatOse = new Gestion_Request_Sunat_Ose();
$nroCola = isset( $_GET['nro_cola'] ) ? $_GET['nro_cola'] : false;
$json_cola =  $gestionRequestSunatOse->getGetStatusJob( $nroCola );
// $log_cola  = "Iniciando cola numero : ".$numCola . " horas: ". date('Y-m-d H:m:s:').round(microtime(true) * 1000). "<br>";
// write_log($local,1);
if ( count( $json_cola )> 0)
{
	$tb_comprobante_id = $json_cola["tb_comprobante_id"];
	$jsonEntrada = json_decode($json_cola['data_cpe']);
	$inicio_cpe_envio =  date('Y-m-d H:m:s:').round(microtime(true) * 1000).'_Nuevo envio de cpe: ID:'.$tb_comprobante_id  ;
	
	$tb_comprobante_all_id =  $json_cola["tb_comprobante_all_id"];
	$tb_emisor_user_sol = trim($json_cola["tb_emisor_user_sol"]);
	$tb_emisor_clave_sol = trim($json_cola["tb_emisor_clave_sol"]);
	$tb_emisor_tipo_operador = $json_cola["tb_emisor_tipo_operador"];
	$tb_comprobante_nomextarccomele = $json_cola['tb_comprobante_nomextarccomele'];

	$tb_comprobante_emailto = $jsonEntrada->correo;
	$nroticket = $jsonEntrada->nroTicket;
	$tb_razon_social = $jsonEntrada->oAdquiriente->apellidosNombresDenominacionRazonSocial;
	$ruc = $json_cola['ruc_emisor'];

	$tipo_cpe =  $jsonEntrada->codigoTipoDocumento;
	$fecha_emision = substr($jsonEntrada->fechaEmision, 0, 10 ) ;
	$montoTotal = $jsonEntrada->montoTotalMonedaOriginal;
	$numeroidentificacionCliente =$jsonEntrada->oAdquiriente->numeroDocumentoIdentidad;
	$fileAnexo =  get_anexo_by_ticket(  $ruc, $nroticket );
	
	$pathFileZipXmlEnvio =   get_path_comprobante(  str_replace('.zip', "", $tb_comprobante_nomextarccomele) ,  'xml' ,  $fecha_emision, $ruc, $tipo_cpe);
	$tb_comprobante_base64zip  = $json_cola["base64zip"];
	$tb_comprobante_base64pdf  = $json_cola["base64pdf"];
	
	// $getDataByCpe = get_ws( $tipo_cpe, $json_cola );
	$tipoMoneda = $jsonEntrada->codigoTipoMoneda;
	$tb_emisor_ws = $tb_emisor_tipo_operador == '0' ? "https://e-factura.sunat.gob.pe/ol-it-wsconscpegem/billConsultService" : $json_cola['tb_emisor_ws'];
	// $tb_emisor_ws =  "https://prod.conose.pe/ol-ti-itcpe/billService.svc" ;
	$directorio_final = get_path_comprobante(  '' ,  'cdr' ,  $fecha_emision, $ruc, $tipo_cpe);
	
	$base64_xml_cpe ='';
	
	
	$parametros =  [
		'tb_emisor_user_sol' => $tb_emisor_user_sol,
		'tb_emisor_clave_sol' => $tb_emisor_clave_sol,
		'tb_comprobante_nomextarccomele' => $tb_comprobante_nomextarccomele,
		'fecha_emision' => $fecha_emision,
		'tb_comprobante_base64zip' => $tb_comprobante_base64zip,
		'tb_emisor_ws' => $tb_emisor_ws,
		'tb_emisor_tipo_operador' => $tb_emisor_tipo_operador,
		'directorio_final' => $directorio_final,//ruta donde se guarda el CDR
		'tipo_cpe' => $tipo_cpe,
		'base64_xml_cpe' => $base64_xml_cpe,
		'tb_comprobante_id' => $tb_comprobante_id,
		'numeroidentificacionCliente' => $numeroidentificacionCliente,
		'montoTotal' => $montoTotal,
		'tb_comprobante_emailto' => $tb_comprobante_emailto,
		'tb_razon_social' => $tb_razon_social,
		'tb_comprobante_base64pdf' => $tb_comprobante_base64pdf,
		'tipo_moneda' => $tipoMoneda,
		'fileAnexo' =>$fileAnexo,
		'tb_comprobante_all_id' => $tb_comprobante_all_id
	];
	#================================================================
	#     Distribuidor depende del destino del ENVIO del
	#      						CPE
	# SUNAT = 0
	# NUBEFACT = 1 
	#================================================================
	
	$res = $gestionRequestSunatOse->cpeReproceso( $parametros, 'getStatus' );
	read_log_cola($tb_comprobante_id, 'w');
	echo json_encode($res, JSON_PRETTY_PRINT);
	$res['inicio_cpe_envio'] = $inicio_cpe_envio;
	// $res['num_cola'] = $numCola;
	if ( $res[ 'response_sunat' ]['respe_sevice']== 'A' || $res[ 'response_sunat' ]['respe_sevice']== 'O' ) 
	{
		try {
			
			$res['registro_tablas_cpe']=$gestionRequestSunatOse->saveCpe( $pathFileZipXmlEnvio,$tipo_cpe, $tb_comprobante_id );	
			$res['ruta_xml_envio'] = $pathFileZipXmlEnvio;
			$res['path_cdr'] = $directorio_final;
			$res['registro_cpe'] = 'se registro un nuevo registro tabla cpe para el tipo de cpe: '.$tipo_cpe.'-'. get_type_doc_by_type($tipo_cpe);
		} catch (Exception $e) {
			$error = 'ERROR de registro tablas CPE:  ' . $e->getMessage();
			write_log( $error );
			$res['registro_cpe'] = $error;
		}
	}else
	{
		$res['registro_cpe'] = 'no se registro un nuevo registro tabla cpe';
	}
	
	$respuesta_full= implode("\n", $res);
	$respuesta_sendgrid= implode("\n", $res['response_email']);
	if ( $res[ 'HTTP_CODE' ] == "500"  ||  $res[ 'HTTP_CODE' ] == "200" ) 
	{
		
		write_log( json_encode($res, JSON_PRETTY_PRINT) , 1);
		// write_log("Repuesta Email: ". $respuesta_sendgrid, 1);
	}
	else
	{
		write_log($respuesta_full);

	}
	
	
}
else {
	
	$res['Respuesta'] =  "No hay CPE para enviar ".date("Y-m-d H:i:s");
	
	echo json_encode($res, JSON_PRETTY_PRINT);
}

