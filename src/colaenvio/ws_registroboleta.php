<?php
 $uri = $_SERVER['REQUEST_URI'];

 header('Content-type: text/plain');

$json = implode(json_decode(file_get_contents('php://input'), true));
$xml_decode =base64_decode($json);


#######################  Migracion XML a BD #####################
/*

Programa que toma Informacion del los XML y los inserta en Base de 
Datos
*/
#################################################################

include "conexPGSQL.php";
error_reporting(E_ERROR);
$cn = new conexPGSQL();


function max_num_id() // Funcion determina Numero Maximo del ID de cabecera
{$cn = new conexPGSQL();
$res = $cn->querys("SELECT MAX(num_id) as num_id FROM sh_cloud_cpe.tb_boleta_cab;");							
while ($row = pg_fetch_assoc($res)){$result = $row; }
return $result['num_id'];
}

function insertar_cab($insert_cab) // Funcion Inserta en Cabecera la Informacion.
{
	$cn = new conexPGSQL();
	$res = $cn->querys($insert_cab);
}


function insertar_det($insert_det) // Funcion Inserta en detalle la Informacion.
{
	$cn = new conexPGSQL();
	$res = $cn->querys($insert_det);
	//echo "Insertando registro de detalles: " . $insert_det;
}

function insertar_notas($insertar_notas) // Funcion Inserta en NOTAS DEL CPE.
{
	$cn = new conexPGSQL();
	$res = $cn->querys($insertar_notas);
}

function insertar_tributos($insertar_tributos) // Funcion Inserta los tributos GENERALES
{
	$cn = new conexPGSQL();
	$res = $cn->querys($insertar_tributos);
}
//$c = simplexml_load_string($a);


//$xml =  simplexml_load_string($b);
//echo $xml_decode;
$xml =  simplexml_load_string($xml_decode);

$num_id = max_num_id()+1; // Invoca a la Funcion para obtener el num_id*/
/*
foreach (glob("*.xml") as $archivos){ 

$xml = simplexml_load_file($archivos);
$num_id = max_num_id()+1; // Invoca a la Funcion para obtener el num_id*/


//*********************************************************************************************************   
//										       CABECERA   
//*********************************************************************************************************   

######################################     DATOS EMISOR  ###################################################

$ruc_emisor = $xml->xpath('cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID')[0];
$tipodocumentoemisor = $xml->xpath('cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID')[0]['schemeID'];
############################################################################################################

######################################     DATOS ADQUIRIENTE  ##############################################
$tipodocumentocliente = $xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID')[0]['schemeID'];
$numeroDocumentoIdentidad = $xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID')[0];
$clie_razonsocial = end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cbc:RegistrationName')[0]);
$clie_ubigeo = end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:ID')[0]);
$urbanizacion = end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CitySubdivisionName')[0]);
$distrito = end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:District')[0]);
$provincia = end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CityName')[0]);
$cod_pais = end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cac:Country/cbc:IdentificationCode')[0]);
$departamento = end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CityName')[0]);
$dir_completa =  end($xml->xpath('cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cac:AddressLine/cbc:Line')[0]);
############################################################################################################

######################################     DETALLES CPE  ###################################################
$numeroCorrelativo = substr($xml->xpath("cbc:ID")[0], 5, 13);
$numeroSerie = substr($xml->xpath("cbc:ID")[0], 0, 4);
$fecha = $xml->xpath("cbc:IssueDate")[0]." ".$xml->xpath("cbc:IssueTime")[0];
$codigoTipoOperacion = end($xml->xpath("cbc:InvoiceTypeCode")[0]['listID']);
$codigoTipoMoneda = end($xml->xpath("cbc:DocumentCurrencyCode")[0]);
############################################################################################################

###############################     DATOS GLOBALES IMPUESTOS, BASE ,TOTALES  ###############################
$montoImpuestoMonedaOriginal = floatval(end($xml->xpath("cac:TaxTotal/cbc:TaxAmount")[0]));
$montoValorVentaMonedaOriginal = floatval(end($xml->xpath("cac:LegalMonetaryTotal/cbc:LineExtensionAmount")[0]));
$montoTotalMonedaOriginal = floatval(end($xml->xpath("cac:LegalMonetaryTotal/cbc:PayableAmount")[0]));
############################################################################################################

###############################     PERCEPCIONES  ###########################################################
$codigoRegimenPercepcion = end($xml->xpath("cac:AllowanceCharge/cbc:AllowanceChargeReasonCode")[0]);
$porcentajePercepcion = end($xml->xpath("cac:AllowanceCharge/cbc:MultiplierFactorNumeric")[0]);
$montoPercepcionMonedaNacional = end($xml->xpath("cac:AllowanceCharge/cbc:Amount")[0]);
$montoBasePercepcionMonedaNacional = end($xml->xpath("cac:AllowanceCharge/cbc:BaseAmount")[0]);
$importeTotalConPercepcionMonedaNacional = end($xml->xpath("cac:PaymentTerms/cbc:Amount")[0]);
#############################################################################################################


###############################     ORDENES DE COMPRA Y GUIAS  ##############################################
$ordenes_compra = end($xml->xpath("cac:OrderReference/cbc:ID")[0]);
$guia_remi = end ($xml->xpath("cac:DespatchDocumentReference/cbc:ID")[0]);
$tipo_doc_guiaremi = end($xml->xpath("cac:DespatchDocumentReference/cbc:DocumentTypeCode")[0]);
#############################################################################################################


###############################  DOCUMENTO RELACIONADO  	   ##############################################
$num_doc_rela = end($xml->xpath("cac:AdditionalDocumentReference/cbc:ID")[0]);
$tipo_doc_rela	 = end($xml->xpath("cac:AdditionalDocumentReference/cbc:DocumentTypeCode")[0]);
$estado_doc_rela	 = end($xml->xpath("cac:AdditionalDocumentReference/cbc:DocumentStatusCode")[0]);
$num_identidad_doc_rela = end($xml->xpath("cac:AdditionalDocumentReference/cac:IssuerParty/cac:PartyIdentification/cbc:ID")[0]);
#############################################################################################################

$insert_cab = "INSERT INTO sh_cloud_cpe.tb_boleta_cab
									values ('$num_id',
											'$ruc_emisor',
											'$numeroSerie',
											'$numeroCorrelativo',
											'$fecha',
											'$codigoTipoOperacion',
											'$codigoTipoMoneda',
											'$tipodocumentocliente',
											'$numeroDocumentoIdentidad',
											'$clie_ubigeo',
											'$clie_razonsocial',
											'$distrito',
											'$provincia',
											'$cod_pais',
											'$departamento',
											'$urbanizacion',
											'$dir_completa',
											'$montoImpuestoMonedaOriginal',
											'$montoValorVentaMonedaOriginal',
											'$montoTotalMonedaOriginal',
											'$codigoRegimenPercepcion',
											'$porcentajePercepcion',
											'$montoPercepcionMonedaNacional',
											'$montoBasePercepcionMonedaNacional',
											'$importeTotalConPercepcionMonedaNacional',
											'$ordenes_compra',
											'$guia_remi',
											'$tipo_doc_guiaremi',
											'$tipo_doc_rela',
											'$estado_doc_rela',
											'$num_doc_rela',
											'$num_identidad_doc_rela')";

insertar_cab($insert_cab);// Invoca a la Funcion para INSERTAR DATA EN CAB

//*********************************************************************************************************   
//										       DETALLE   
//*********************************************************************************************************   
$nroOrden = 1;
unset($item_cod,$item_cod_sunat);

						foreach ($xml->xpath("cac:InvoiceLine") as $nodo) {

// OBTIENE UNIDAD DE MEDIDA DEL ITEM			
							$InvoicedQuantity = $nodo->xpath("cbc:InvoicedQuantity");					
							$cantidad = $InvoicedQuantity[0];
                   			$codigoUnidadMedida = $cantidad["unitCode"];
                   			$Item = $nodo->xpath("cac:Item");
                   			foreach ($Item as $nodoProci) {
                   				$Description = $nodoProci->xpath("cbc:Description");
                   				$item_des = $Description[0];
        //OBTIENE CODIGO DE SUNAT
                   				foreach ($nodoProci->xpath("cac:CommodityClassification/cbc:ItemClassificationCode") as $nodoClassificationCode) {
                   					$item_cod_sunat = end($nodoClassificationCode[0]);
                   				}
								if(strlen($item_cod_sunat) < 1){$item_cod_sunat = null;}
        //OBTIENE CODIGO DE PRODUCTO
                   				foreach ($nodoProci->xpath("cac:SellersItemIdentification/cbc:ID") as $nodocbcID) {
                   					$item_cod = end($nodocbcID[0]);
                   				}
								
								if(strlen($item_cod) < 1){$item_cod = null;}
								//echo $item_cod;
								//echo"<br>";
        //echo $descrip;		
                   			}
							foreach ($nodo->xpath("cac:PricingReference/cac:AlternativeConditionPrice/cbc:PriceAmount ") as $nodoPriceAmount) {
                   				$PriceAmountXX = floatval(end($nodoPriceAmount[0]));
                   			}
	// OBTIENE PRECIO DEL PRODUCTO SI IGV	
                   			foreach ($nodo->xpath("cac:Price") as $nodoPrice) {
                   				$montoValorVentaUnitarioMonedaOriginal = floatval(end($nodoPrice->xpath("cbc:PriceAmount ")[0]));
                   			}
					

	// OBTIENE 	EL VALOR DE LA VENTA POR ITEM 					
						$montoValorVentaTotalMonedaOriginal = end($nodo->xpath("cbc:LineExtensionAmount")[0]); 
  //codigoTipoAfectacionIGV
//    //OBTENCION DE IMPUESTOS
                   			$TaxTotal = $nodo->xpath("cac:TaxTotal");
                   			foreach ($TaxTotal as $nodoTaxTotal) {
        //codigoTipoAfectacionIGV
                   				foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cac:TaxCategory/cbc:TaxExemptionReasonCode") as $nodoTaxExemptionReasonCode) {
                   					$TaxExemptionReasonCode = end($nodoTaxExemptionReasonCode[0]);
                   				}
        //montoBaseMonedaOriginal
                   				foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cbc:TaxAmount ") as $nodoTaxAmount) {
                   					$montoTotalMonedaOriginalXX = floatval(end($nodoTaxAmount[0]));
                   				}
		// CODIGO INTERNACIONAL DEL TRIBUTO						
                   				foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode") as $nodoTaxTypeCode) {
                   					$codigoInternacionalXX = end($nodoTaxTypeCode[0]);
                   				}
	// BASE IMPONIBLE DEL ITEM							
                   				foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cbc:TaxableAmount ") as $nodoTaxableAmount) {
                   					$montoBaseMonedaOriginalXX = floatval(end($nodoTaxableAmount[0]));
                   				}
  //OBTIENE NOMBRE DEL TRIBUTO Y CODIGO DEL TRINBUTO
								foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme") as $nodoTaxScheme) {
                   					$nombreXX = end($nodoTaxScheme->xpath("cbc:Name")[0]);
                   					$IDXX = end($nodoTaxScheme->xpath("cbc:ID")[0]);
                   				}
// PORCENTAJE DEL TRIBUTO
                   				foreach ($nodoTaxTotal->xpath("cac:TaxSubtotal/cac:TaxCategory") as $nodoTaxCategory) {
                   					$PercentXX = floatval(end($nodoTaxCategory->xpath("cbc:Percent")[0]));
                   				}
                   			}
                   			$nroOrden++;
							$insert_det = "INSERT INTO sh_cloud_cpe.tb_boleta_det (num_id,item_des,item_cod,item_cod_sunat,item_und,item_cant,item_pu,item_mnt_unt,item_mnt_valor_total,imp_cod_afec_trib,imp_mnt_total_trib,imp_mnt_base,imp_nombre,imp_cod_inter,imp_cod_trib,imp_porc)
												values ('$num_id',
														'$item_des',
														'$item_cod',
														'$item_cod_sunat',
														'$codigoUnidadMedida',
														'$cantidad',
														'$PriceAmountXX',
														'$montoValorVentaUnitarioMonedaOriginal',
														'$montoValorVentaTotalMonedaOriginal',
														'$TaxExemptionReasonCode',
														'$montoTotalMonedaOriginalXX',
														'$montoBaseMonedaOriginalXX',
														'$nombreXX',
														'$codigoInternacionalXX',
														'$IDXX',
														'$PercentXX')";
														insertar_det($insert_det);
                   		
						
						}
//*********************************************************************************************************   
//										       NOTAS   
//*********************************************************************************************************  

                  
                   		foreach ($xml->xpath("cbc:Note") as $nodoNote) {
                   			$descripcion_nota = end($nodoNote[0]);
                   			$codigo_nota = end($nodoNote[0]['languageLocaleID']);
							
							$insertar_notas = "INSERT INTO sh_cloud_cpe.tb_boleta_notas (num_id,codigo,descripcion)
												values ('$num_id',
														'$codigo_nota',
														'$descripcion_nota')";
														insertar_notas($insertar_notas);
														
													
                   		}

//*********************************************************************************************************   
//										     TRIBUTO GENERALES   
//*********************************************************************************************************  

                   		
                   		foreach ($xml->xpath("cac:TaxTotal/cac:TaxSubtotal") as $nodo_tri_general) {
							
                   		$montoTotaltributo = floatval(end($nodo_tri_general->xpath("cbc:TaxAmount")[0]));
						$montoBaseMonedaOriginal = floatval(end($nodo_tri_general->xpath("cbc:TaxableAmount")[0]));
						foreach ($nodo_tri_general->xpath("cac:TaxCategory/cac:TaxScheme") as $nodo_TaxScheme) {
						$nombre = end($nodo_TaxScheme->xpath("cbc:Name")[0]);	
						$codigoInternacional = end($nodo_TaxScheme->xpath("cbc:TaxTypeCode")[0]);	
						$codigo_impuesto = end($nodo_TaxScheme->xpath("cbc:ID")[0]);	
						
						

						
							$insertar_tributos = "INSERT INTO sh_cloud_cpe.tb_boleta_tri (num_id,imp_mnt_total_tri,imp_mnt_base,imp_nombre,imp_cod_inter,imp_cod_trib)
												values ('$num_id',
														'$montoTotaltributo',
														'$montoBaseMonedaOriginal',
														'$nombre',
														'$codigoInternacional',
														'$codigo_impuesto')";
								insertar_tributos($insertar_tributos);
						}		
							

	}
	

$Respuesta = json_encode(array("Respuesta" => "Se ha Insertado en db_cpe el Comprobante $ruc_emisor-03-$numeroSerie-$numeroCorrelativo"));
echo $Respuesta;  



